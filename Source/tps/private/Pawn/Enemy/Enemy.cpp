// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Enemy/Enemy.h"
#include "Engine/SkeletalMesh.h"
#include "UObject/ConstructorHelpers.h"
#include "Pawn/Enemy/EnemyComponent/EnemyAttackComonent.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Managers/GameItemManager.h"
#include "Weapon/WeaponBase.h"
#include "Pawn/Enemy/EnemyAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/ArrowComponent.h"
// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ConstructorHelpers::FObjectFinder<USkeletalMesh> skeletalMesh(TEXT("Engine.SkeletalMesh'/Game/Characters/Mannequins/Meshes/SKM_Manny.SKM_Manny'"));
	{
		GetMesh()->SetSkeletalMesh(skeletalMesh.Object);
		GetMesh()->SetRelativeLocationAndRotation(FVector(0, 0, -90), FRotator(0, -90, 0));
	}
	UEnemyAttackComonent* attackComp = CreateDefaultSubobject<UEnemyAttackComonent>(TEXT("AttackComponent"));
	_dicPairComponent.Add(UEnemyAttackComonent::StaticClass(), attackComp);
}

float AEnemy::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	FTimerHandle onHitReset;
	GetWorld()->GetTimerManager().SetTimer(onHitReset,this, &AEnemy::HitReset,0.5f,false);
	if(!isHit)
	{
		OnCharacterHit.Broadcast(true);
		UE_LOG(LogTemp, Warning, TEXT("HitDamage : %d"), Damage);
	}
	return Damage;
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle UnusedHandle;
	GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &AEnemy::Initialize, 0.5f, false);
	aiController=Cast<AEnemyAIController>(GetController());
	//sightArrow = getcomponentbyna<UArrowComponent>(TEXT("asd"));
}

void AEnemy::Initialize()
{
	UGameInstance* gameinstance = Owner->GetGameInstance<UGameInstance>();
	UServiceLocatorServeSystem* serviceLocator = gameinstance->GetSubsystem<UServiceLocatorServeSystem>();
	AGameItemManager* itemManager = serviceLocator->GetService<AGameItemManager>();
	AWeaponBase* weapon = itemManager->GetWeaponFromPool<AWeaponBase>(EWeaponType::Rifle);
	UEnemyAttackComonent* attackComp = GetCharacterComponent<UEnemyAttackComonent>();
	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	weapon->AttachToComponent(GetMesh(), AttachmentRules, "WeaponSoket");
	attackComp->SetCurrentWeapon(weapon);
	weapon->SetOwner(this);
	weapon->InitializeWeapon();
}
void AEnemy::HitReset()
{
	isHit=false;
}
FVector AEnemy::GetFireEndCheckVector(int32 dir)
{
	UObject* targetobject = aiController->GetBlackBoard()->GetValueAsObject("EnemyActor");
	FVector targetlocation = Cast<AActor>(targetobject)->GetActorLocation();
	FVector direction = targetlocation - GetActorLocation();
	return GetActorLocation() + (direction * dir);
}
FVector AEnemy::GetFireEndCheckStartPos()
{
	return sightArrow->GetComponentLocation();
}
