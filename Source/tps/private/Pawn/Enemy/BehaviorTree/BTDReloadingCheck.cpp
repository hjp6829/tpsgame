// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Enemy/BehaviorTree/BTDReloadingCheck.h"
#include "Pawn/Enemy/Enemy.h"
#include "Pawn/Enemy/EnemyComponent/EnemyAttackComonent.h"
#include "Weapon/WeaponBase.h"
#include "BehaviorTree/BlackboardComponent.h"
UBTDReloadingCheck::UBTDReloadingCheck()
{
	NodeName="BTD_ReloadingCheck";
}

bool UBTDReloadingCheck::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{	
	UObject* temp = OwnerComp.GetBlackboardComponent()->GetValueAsObject("SelfActor");
	AEnemy* enemy = Cast<AEnemy>(temp);
	UEnemyAttackComonent* attackComp = enemy->GetCharacterComponent<UEnemyAttackComonent>();
	if(attackComp->NowReloadingCheck())
		return true;
	return false;
}
