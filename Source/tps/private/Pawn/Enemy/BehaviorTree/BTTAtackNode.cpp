// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Enemy/BehaviorTree/BTTAtackNode.h"
#include "AIController.h"
#include "Pawn/CustomCharacter.h"
#include "Pawn/Enemy/EnemyComponent/EnemyAttackComonent.h"
UBTTAtackNode::UBTTAtackNode()
{
	NodeName="BTT_Attack";
	bNotifyTick=true;

}

EBTNodeResult::Type UBTTAtackNode::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ACustomCharacter* characterOwner = Cast<ACustomCharacter>(OwnerComp.GetAIOwner()->GetPawn());
	attackComp = characterOwner->GetCharacterComponent<UEnemyAttackComonent>();
	GetWorld()->GetTimerManager().SetTimer(OnAttackEndTimer,this,&UBTTAtackNode::SetAttackEnd,FMath::FRandRange(attackTimeMin, attackTimeMax),false);
	treeComp= &OwnerComp;
	attackComp->SetNowAim(true);
	GetWorld()->GetTimerManager().SetTimer(onAttackTimerHandle,this,&UBTTAtackNode::AttackLoof,0.1f,true);
	return EBTNodeResult::InProgress;
}

void UBTTAtackNode::SetAttackEnd()
{
	attackComp->SetNowAim(false);
	GetWorld()->GetTimerManager().ClearTimer(onAttackTimerHandle);
	treeComp->OnTaskFinished(this, EBTNodeResult::Succeeded);
}

void UBTTAtackNode::AttackLoof()
{
	attackComp->SetWeaponState(true);
}
