// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Enemy/BehaviorTree/BTTRandomRange.h"

UBTTRandomRange::UBTTRandomRange()
{	
	NodeName = "BTT_RandomRange";
	bNotifyTick = true;
}

EBTNodeResult::Type UBTTRandomRange::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	int32 value = FMath::RandRange(0,100);
	if(value<= Probability)
		return EBTNodeResult::Succeeded;
	else
		return EBTNodeResult::Failed;
}
