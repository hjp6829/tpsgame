// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Enemy/EnemyAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/BehaviorTree.h"
#include "Perception/AIPerceptionComponent.h"
AEnemyAIController::AEnemyAIController()
{
	behaviortreeComp=CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	blackboard=CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackBoardComp"));
}

void AEnemyAIController::OnPossess(APawn* pawn)
{
	Super::OnPossess(pawn);
	ownerPawn=pawn;
	if (blackboard)
	{
		blackboard->InitializeBlackboard(*tree->BlackboardAsset);
	}
}

UBlackboardComponent* AEnemyAIController::GetBlackBoard()
{
	return blackboard;
}

void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();
	/*RunBehaviorTree(tree);
	behaviortreeComp->StartTree(*tree);
	blackboard->SetValueAsObject("SelfActor",ownerPawn);
	GetPerceptionComponent()->OnTargetPerceptionUpdated.AddDynamic(this,&AEnemyAIController::OntargetDetected);*/
}

void AEnemyAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	DistanceCalculation();
}

void AEnemyAIController::OntargetDetected(AActor* actor, FAIStimulus const Stimulus)
{
	if (actor->ActorHasTag(FName("Player")))
	{
		if(Stimulus.WasSuccessfullySensed())
		{
			///���߿� �Ő���� timer���� �߰��ؾ���
			isTargetDetected=true;
			blackboard->SetValueAsBool("HasLineOfSight",true);
			blackboard->SetValueAsObject("EnemyActor", actor);
			SetFocus(actor, EAIFocusPriority::Gameplay);
			targetActor = Cast<AActor>(blackboard->GetValueAsObject("EnemyActor"));
			//GetWorld()->GetTimerManager().SetTimer(UpdateDistancEWeaponSlotle, this, &AEnemyAIController::DistanceCalculation, 0.1f, true);
		}
		else
		{
			//GetWorld()->GetTimerManager().ClearTimer(UpdateDistancEWeaponSlotle);
			isTargetDetected=false;
			targetActor=nullptr;
			blackboard->SetValueAsBool("HasLineOfSight", false);
			blackboard->SetValueAsObject("EnemyActor", nullptr);
			ClearFocus(EAIFocusPriority::Gameplay);
		}
	}
}

void AEnemyAIController::DistanceCalculation()
{
	if(!isTargetDetected)
	return;
	float dis = FVector::Distance(ownerPawn->GetActorLocation(),targetActor->GetActorLocation());
	blackboard->SetValueAsFloat("TargetDistance", dis);
}
