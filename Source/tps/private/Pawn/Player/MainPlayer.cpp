// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/MainPlayer.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/SkeletalMesh.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
#include "Pawn/Player/PlayerComponent/PlayerAttackComponent.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "UI/Player/PlayerHUD.h"
#include "Components/CapsuleComponent.h"
#include "Item/ItemBox.h"
#include "Pawn/Player/PlayerComponent/PlayerStateComponent.h"
#include "Pawn/Player/PlayerComponent/PlayerMoveComponent.h"
#include "Pawn/Player/PlayerComponent/InteractionComponent.h"
#include "Managers/UIManagerServeSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Pawn/Player/PlayerComponent/BuildSystemComponent.h"
// Sets default values
AMainPlayer::AMainPlayer()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ConstructorHelpers::FObjectFinder<USkeletalMesh> skeletalMesh(TEXT("Engine.SkeletalMesh'/Game/Characters/Mannequins/Meshes/SKM_Manny.SKM_Manny'"));
	{
		GetMesh()->SetSkeletalMesh(skeletalMesh.Object);
		GetMesh()->SetRelativeLocationAndRotation(FVector(0,0,-90),FRotator(0,-90,0));
	}
	springArmComp=CreateDefaultSubobject<USpringArmComponent>(TEXT("playerSpringArm"));
	springArmComp->SetupAttachment(RootComponent);
	springArmComp->SetRelativeLocation(FVector(0,0,80));
	springArmComp->bUsePawnControlRotation=true;
	springArmComp->TargetArmLength=400;

	playerCameraComp=CreateDefaultSubobject<UCameraComponent>(TEXT("playerCamera"));
	playerCameraComp->SetupAttachment(springArmComp, USpringArmComponent::SocketName);
	playerCameraComp->bUsePawnControlRotation = false;

	bUseControllerRotationRoll=false;
	bUseControllerRotationYaw= false;
	bUseControllerRotationPitch=false;
	
	ConstructorHelpers::FObjectFinder<UInputMappingContext> DefaultMapping(TEXT("EnhancedInput.InputMappingContext'/Game/Input/PlayerInput.PlayerInput'"));
	if (DefaultMapping.Succeeded())
	{
		defaultContext = DefaultMapping.Object;
	}


	
#pragma region Components
	UPlayerStateComponent* stateComp = CreateDefaultSubobject<UPlayerStateComponent>(TEXT("PlayerStateComponent"));
	_dicPairComponent.Add(UPlayerStateComponent::StaticClass(), stateComp);
	UPlayerInputComonent* inputComp = CreateDefaultSubobject<UPlayerInputComonent>(TEXT("InputComponent"));
	_dicPairComponent.Add(UPlayerInputComonent::StaticClass(), inputComp);
	UPlayerAttackComponent* attackComp= CreateDefaultSubobject<UPlayerAttackComponent>(TEXT("AttackComponent"));
	_dicPairComponent.Add(UPlayerAttackComponent::StaticClass(), attackComp);
	UPlayerInventoryComoponent* inventoryComp = CreateDefaultSubobject<UPlayerInventoryComoponent>(TEXT("InventoryComponent"));
	_dicPairComponent.Add(UPlayerInventoryComoponent::StaticClass(), inventoryComp);
	UPlayerMoveComponent* moveComp = CreateDefaultSubobject<UPlayerMoveComponent>(TEXT("MoveComponent"));
	_dicPairComponent.Add(UPlayerMoveComponent::StaticClass(), moveComp);
	UInteractionComponent* interactionComp = CreateDefaultSubobject<UInteractionComponent>(TEXT("InteractionComp"));
	_dicPairComponent.Add(UInteractionComponent::StaticClass(), interactionComp);
	buildComp = CreateDefaultSubobject<UBuildSystemComponent>(TEXT("BuildComp"));
	_dicPairComponent.Add(UBuildSystemComponent::StaticClass(), buildComp);
#pragma endregion
}

// Called when the game starts or when spawned
void AMainPlayer::BeginPlay()
{
	Super::BeginPlay();	
	GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AMainPlayer::OnOverlapEnd);
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AMainPlayer::OnOverlapBegin);
	if (APlayerController* playerController = Cast<APlayerController>(GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* SubSystem =
			ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(playerController->GetLocalPlayer()))
			SubSystem->AddMappingContext(defaultContext, 0);
	}
	UPlayerInputComonent* inputComp = GetCharacterComponent<UPlayerInputComonent>();
	inputComp->OnMouseAim.AddUObject(this,&AMainPlayer::SetHUDCrossHair);
	inputComp->OnMouseAim.AddUObject(this, &AMainPlayer::SetSpringArmAim);
	inputComp->OnCover.AddUObject(this, & AMainPlayer::SetIsCover);
	inputComp->OnCronuch.AddUObject(this, &AMainPlayer::SetIsCronch);

	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UUIManagerServeSystem* uimanager = gameInstance->GetSubsystem<UUIManagerServeSystem>();
	UPlayerHUD* hud = uimanager->GetUI<UPlayerHUD>(EUIType::HUD);
	playerHUD= hud;
	playerHUD->SetOwnerPlayer(this);
}

// Called every frame
void AMainPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMainPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	if (UEnhancedInputComponent* EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerInputComponent))
	{
		OnInputBindingDelegate.Broadcast(EnhancedInputComponent);
	}
}

FVector AMainPlayer::GetFireEndCheckVector(int32 dir)
{
	return playerCameraComp->GetComponentLocation() + playerCameraComp->GetForwardVector() * dir;
}

FVector AMainPlayer::GetFireEndCheckStartPos()
{
	return playerCameraComp->GetComponentLocation();
}
void AMainPlayer::SetHUDCrossHair(bool value)
{
	playerHUD->CrosshairVisibility(value);
}

void AMainPlayer::SetSpringArmAim(bool value)
{
	if (value)
	{
		if (isCronch && !isCover)
		{
			springArmComp->TargetArmLength = 50;
			springArmComp->SocketOffset = FVector(0, 55, -40);
			playerCameraComp->SetRelativeRotation(FRotator(0, -5, 0));
			return;
		}
		springArmComp->TargetArmLength=50;
		springArmComp->SocketOffset=FVector(0,65,0);
		playerCameraComp->SetRelativeRotation(FRotator(0,-5,0));
	}
	else
	{
		springArmComp->TargetArmLength = 400;
		springArmComp->SocketOffset = FVector(0, 0, 0);
		playerCameraComp->SetRelativeRotation(FRotator(0, 0, 0));
	}
}

void AMainPlayer::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	switch (OtherComp->GetCollisionObjectType())
	{
	case ECollisionChannel::ECC_GameTraceChannel5:
		OnCharacterOverlapHideWall.Broadcast(true);
		break;
	default:
		break;
	}

	IInteractable* Interactable = Cast<IInteractable>(OtherActor);
	if(Interactable)
		Interactable->Interact(this);
}

void AMainPlayer::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	
}

float AMainPlayer::TakeDamage(float Damage, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	UE_LOG(LogTemp, Warning, TEXT("playerHit"));
	return 0.0f;
}

void AMainPlayer::TogglePlayerInventory()
{
	playerHUD->TogglePlayerInventory();
}

void AMainPlayer::AddCameraRotation(FRotator rotation)
{
	playerCameraComp->AddLocalRotation(rotation);
}

FVector AMainPlayer::GetMeleeAttackLocation()
{
	return GetMesh()->GetSocketLocation("WeaponSoket");
}

void AMainPlayer::SetIsCover(bool value)
{
	isCover=value;
}

void AMainPlayer::SetIsCronch(bool value)
{
	isCronch = value;
}

UPlayerHUD* AMainPlayer::GetPlayerHUD()
{
	return playerHUD;
}



