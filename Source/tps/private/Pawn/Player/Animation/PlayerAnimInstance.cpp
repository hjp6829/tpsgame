// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/Animation/PlayerAnimInstance.h"
#include "Pawn/CustomCharacter.h"
#include "Pawn/Player/PlayerComponent/PlayerMoveComponent.h"
#include "Components/CapsuleComponent.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"

void UPlayerAnimInstance::NativeBeginPlay()
{
	Super::NativeBeginPlay();
	ownerCharacter= Cast<ACustomCharacter>(TryGetPawnOwner());
	ownerCharacter->OnSetCharacterYawAndPitch.AddUObject(this,&UPlayerAnimInstance::SetPlayerYawAndPitch);
	ownerCharacter->OnCharacterHit.AddUObject(this, &UPlayerAnimInstance::SetHit);
	UPlayerMoveComponent* moveComp = ownerCharacter->GetCharacterComponent<UPlayerMoveComponent>();
	if(moveComp)
	{moveComp->OnCoverCheck.AddUObject(this,&UPlayerAnimInstance::SetCover);}
	UPlayerInputComonent* inputComp = ownerCharacter->GetCharacterComponent<UPlayerInputComonent>();
	if(inputComp)
	{
		inputComp->OnMouseAim.AddUObject(this, &UPlayerAnimInstance::SetIsAim);
		inputComp->OnCronuch.AddUObject(this, &UPlayerAnimInstance::SetCrouch);
	}
}
void UPlayerAnimInstance::SetPlayerYawAndPitch(float yaw, float pitch)
{
	AimYaw=yaw;
	AimPitch=pitch;
}
void UPlayerAnimInstance::SetCrouch(bool value)
{
	isCrouch=value;
	//UCapsuleComponent* capsuleComp = ownerCharacter->GetCapsuleComponent();
	//FVector newCapsulePos= capsuleComp->GetRelativeLocation();
	//if (isCrouch)
	//{
	//	newCapsulePos.Z += (34 - 88) / 2;
	//	capsuleComp->SetRelativeLocation(newCapsulePos);
	//	capsuleComp->SetCapsuleSize(34,44);
	//	return;
	//}
	//capsuleComp->SetCapsuleSize(34, 88);
	//newCapsulePos.Z += (34 + 88) / 2;
	//capsuleComp->SetRelativeLocation(newCapsulePos);
}

void UPlayerAnimInstance::SetIsAim(bool value)
{
	isAim = value;
	if(isCover&& isAim)
	{
		isCrouch=false;
		return;
	}
	else if (isCover && !isAim)
	{
		isCrouch = true;
	}
}

void UPlayerAnimInstance::SetCover(bool value)
{
	isCover=value;
	if(isCover&&!isCrouch)
		isCrouch=true;
	else if(!isCover && isCrouch)
		isCrouch = false;
		
}

void UPlayerAnimInstance::SetHit(bool value)
{
	isHit= value;
	Montage_Play(hitMontage);
}

void UPlayerAnimInstance::AnimNotify_AttackStart()
{
	OnMeleeAttackCollision.Broadcast(true);
}

void UPlayerAnimInstance::AnimNotify_AttackEnd()
{
	//OnMeleeAttackCollision.Broadcast(false);
}

void UPlayerAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);
	if (ownerCharacter)
	{
		FVector velocity = ownerCharacter->GetVelocity();
		FVector forwardVector = ownerCharacter->GetActorForwardVector();
		Speed = FVector::DotProduct(forwardVector, velocity);
		FVector rightVector = ownerCharacter->GetActorRightVector();
		Direction = FVector::DotProduct(rightVector, velocity);
	}
}


