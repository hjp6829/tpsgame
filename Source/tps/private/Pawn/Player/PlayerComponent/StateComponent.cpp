// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/PlayerComponent/StateComponent.h"


void UStateComponent::SetIsAim(bool value)
{
	isAim=value;
	if(isAim)
	{
		OnRunToggle.Broadcast(false);
		return;
	}
	if(isRun)
		OnRunToggle.Broadcast(true);
}

void UStateComponent::SetIsRun(bool value)
{
	isRun = value;
	if(!isAim)
		OnRunToggle.Broadcast(value);
}

void UStateComponent::HillCharacter(int32 value)
{
	if(currentHP+value>maxHp)
	{
		currentHP=maxHp;
		OnHPUpdate.Broadcast(maxHp, currentHP);
		return;
	}
	currentHP+=value;
	OnHPUpdate.Broadcast(maxHp, currentHP);
}

int32 UStateComponent::GetCharacterGold()
{
	return gold;
}

int32 UStateComponent::GetMaxHP()
{
	return maxHp;
}

int32 UStateComponent::GetHP()
{
	return currentHP;
}
