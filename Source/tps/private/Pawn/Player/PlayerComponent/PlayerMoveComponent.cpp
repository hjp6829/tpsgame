// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/PlayerComponent/PlayerMoveComponent.h"
#include "Pawn/Player/MainPlayer.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
#include "Pawn/Player/PlayerComponent/PlayerStateComponent.h"
#include "Math/RotationMatrix.h"

// Sets default values for this component's properties
UPlayerMoveComponent::UPlayerMoveComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
	// ...
}

void UPlayerMoveComponent::SetRunAndWalkSpeed(float newWalkSpeed, float newRunSpeed)
{
	walkSpeed= newWalkSpeed;
	runSpeed= newRunSpeed;
}


// Called when the game starts
void UPlayerMoveComponent::BeginPlay()
{
	Super::BeginPlay();
	characterMoveComp->MaxWalkSpeed= walkSpeed;
	UPlayerStateComponent* stateComp =player->GetCharacterComponent<UPlayerStateComponent>();
	stateComp->OnRunToggle.AddUObject(this,&UPlayerMoveComponent::RunStateChange);
	UPlayerInputComonent* inputComp = player->GetCharacterComponent<UPlayerInputComonent>();
	inputComp->OnCover.AddUObject(this,&UPlayerMoveComponent::WallTrace);
	inputComp->OnMoveCharacter.AddUObject(this,&UPlayerMoveComponent::MoveCharacter);
	inputComp->OnMouseAim.AddUObject(this, &UPlayerMoveComponent::SetIsAim);
	// ...
}


void UPlayerMoveComponent::InitializeComponent()
{
	Super::InitializeComponent();
	player = Cast<AMainPlayer>(GetOwner());
	characterMoveComp = player->GetCharacterMovement();
}

void UPlayerMoveComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	FVector ownerpos = GetOwner()->GetActorLocation();
	FVector endPos = ownerpos + GetOwner()->GetActorForwardVector() * 100;
}

void UPlayerMoveComponent::RunStateChange(bool value)
{
	if (value)
	{
		characterMoveComp->MaxWalkSpeed = runSpeed;
		return;
	}
	characterMoveComp->MaxWalkSpeed = walkSpeed;
}

void UPlayerMoveComponent::WallTrace(bool value)
{
	if(!value)
	{
		isInCover=false;
		characterMoveComp->SetPlaneConstraintEnabled(false);
		OnCoverCheck.Broadcast(isInCover);
		return;
	}

	FHitResult hitResult;
	FCollisionQueryParams params;
	ECollisionChannel channel = ECC_GameTraceChannel6;
	FVector ownerpos= GetOwner()->GetActorLocation();
	FVector endPos= ownerpos+ GetOwner()->GetActorForwardVector()*100;
	isInCover = GetWorld()->LineTraceSingleByChannel(hitResult, ownerpos, endPos, channel, params);
	OnCoverCheck.Broadcast(isInCover);
	if (isInCover)
	{
		characterMoveComp->SetPlaneConstraintEnabled(isInCover);
		characterMoveComp->SetPlaneConstraintNormal(hitResult.Normal);
		FVector planeNormal = characterMoveComp->GetPlaneConstraintNormal();
		FVector actorPos= hitResult.GetActor()->GetActorLocation();
		FVector dir, newActorPos;
		if (planeNormal.X != 0)
		{
			newActorPos =FVector(actorPos.X, player->GetActorLocation().Y, player->GetActorLocation().Z);
		}
		else if (planeNormal.Y != 0)
		{
			newActorPos = FVector(player->GetActorLocation().X, actorPos.Y, player->GetActorLocation().Z);
		}
		dir = (newActorPos - player->GetActorLocation()).GetSafeNormal();
		FRotator newRotation= dir.Rotation();
		player->SetActorRotation(newRotation);
		return;
	}
}

void UPlayerMoveComponent::MoveCharacter(FVector2D value)
{
	if (player->GetController() != nullptr)
	{
		// find out which way is forward
		const FRotator Rotation = player->GetController()->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// get right vector 
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement 
	
		if(!isInCover)
		{
			player->AddMovementInput(ForwardDirection, value.Y);
			player->AddMovementInput(RightDirection, value.X);
		}
		else
		{
			CoverTrace(value);
		}
	}
}
//라인트레이스사용해도 괜찮지만 마우스회전으로 다른 방향을 보면 라인트래이스는 false값을 가지니까 makefromx를 사용
void UPlayerMoveComponent::CoverTrace(FVector2D playeRightVector)
{
	FVector ownerpos = GetOwner()->GetActorLocation();
	//엄폐물의 평면법선
	FVector planeNormal= characterMoveComp->GetPlaneConstraintNormal();
	//평면법선을 가지고오면 오브젝트->플레이어방향이니까 -1을 곱해줘서 플레이어->오브젝트방향으로 만들어줌
	FVector planeOppositeNormal = planeNormal * -1;
	//x축을 기준으로 회전행렬을 생성해서 백터가 x축에 정렬되도록 해줌
	//예를들어 위,아래,대각선등 x축에 정확히 정렬이 되어있지 않더라도 값을 조정해서 정렬되게 해줌
	FVector planesRightVector;
	FVector planesLeftVector;
	//직사각형일떄 앞,뒤는 정상적으로 작동했지만 좌,우는 정상적으로 작동하지 않아서 앞,뒤 백터를 추가
	if(planeNormal.X!=0)
	{
		planesRightVector = planeOppositeNormal.RightVector;
		planesLeftVector = planeNormal.LeftVector;

	}
	else if (planeNormal.Y != 0)
	{
		planesRightVector = planeOppositeNormal.BackwardVector;
		planesLeftVector = planeNormal.ForwardVector;
	}
	//플레이어 콜라이더 크기만큼 좌,우로 보내줌
	planesRightVector *= 45;
	planesLeftVector *= 45;
	FVector rightOwnerpos, rightEndPos, leftOwnerpos, leftEndPos;
	if (planeNormal.X < 0|| planeNormal.Y < 0)
	{
		rightOwnerpos = ownerpos + planesRightVector;
		rightEndPos = (planeOppositeNormal * 200) + rightOwnerpos;
		leftOwnerpos = ownerpos + planesLeftVector;
		leftEndPos = (planeOppositeNormal * 200) + leftOwnerpos;
	}
	else if (planeNormal.X > 0|| planeNormal.Y > 0)
	{
		rightOwnerpos = ownerpos + planesLeftVector;
		rightEndPos = (planeOppositeNormal * 200) + rightOwnerpos;
		leftOwnerpos = ownerpos + planesRightVector;
		leftEndPos = (planeOppositeNormal * 200) + leftOwnerpos;
	}
	DrawDebugLine(GetWorld(),leftOwnerpos,leftEndPos,FColor::Red,  false, 3,  0, 1);
	DrawDebugLine(GetWorld(), rightOwnerpos, rightEndPos, FColor::Blue, false, 3, 0, 1);
	FHitResult hitRightResult;
	FHitResult hitLeftResult;
	FCollisionQueryParams params;
	ECollisionChannel channel = ECC_GameTraceChannel6;
	bool isRightHit = GetWorld()->LineTraceSingleByChannel(hitRightResult, rightOwnerpos, rightEndPos, channel, params);
	bool isLeftHit = GetWorld()->LineTraceSingleByChannel(hitLeftResult, leftOwnerpos, leftEndPos, channel, params);
	if (isRightHit && isLeftHit)
	{
		if(playeRightVector.X != 0)
		{
#pragma region cylindrical
			//원통형 물체에서 작동하기 위해서 필요함
			//없으면 원통에서 수평으로만 움직이고 있어야지 원통을 따라가듯이 부드럽게 움직임
			FHitResult hitDirResult;
			FVector dirPlaneNormal = planeNormal * -1;
			FVector dirend = (dirPlaneNormal * 200) + ownerpos;
			bool isDirHit = GetWorld()->LineTraceSingleByChannel(hitDirResult, ownerpos, dirend, channel, params);
#pragma endregion
			if (isDirHit)
			{
				characterMoveComp->SetPlaneConstraintNormal(hitDirResult.Normal);
				FRotator tempRotator = FRotator(0, player->GetController()->GetControlRotation().Yaw, 0);
				FVector MyRightVector = tempRotator.Vector().RotateAngleAxis(90, FVector::UpVector);
				player->AddMovementInput(MyRightVector, playeRightVector.X);
			}
		}
	}
	else
	{
		FRotator tempRotator = FRotator(0, player->GetController()->GetControlRotation().Yaw, 0);
		//플레이어가 바라보는 방형의 오른쪽 백터 생성
		FVector MyRightVector = tempRotator.Vector().RotateAngleAxis(90, FVector::UpVector);
		//오른쪽으로 움직이면 1 왼쪽으로 움직이면 -1값
		float SignedValue = FMath::Sign(playeRightVector.X);
		bool selectBool=(SignedValue==1)? isRightHit:isLeftHit;
		float movePower=0;
		if (SignedValue != 0 && selectBool)
		{
			movePower= playeRightVector.X;
		}
		player->AddMovementInput(MyRightVector, movePower);
	}
}

void UPlayerMoveComponent::SetIsAim(bool value)
{
	isAim=value;
	//if(isAim&& isInCover)
	//{
	//	OnCoverCheck.Broadcast(false);
	//}
	//else if(!isAim)
	//	WallTrace(true);
}

