// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
#include "Pawn/Player/MainPlayer.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "InputTriggers.h"
#include "InputActionValue.h"
#include "Pawn/Player/PlayerComponent/PlayerStateComponent.h"
#include "Pawn/Player/PlayerComponent/InteractionComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/GameModeBases/MainGameModeBase.h"
#include "Pawn/Player/PlayerComponent/PlayerMoveComponent.h"
#include "Pawn/Player/PlayerComponent/PlayerAttackComponent.h"
// Sets default values for this component's properties
UPlayerInputComonent::UPlayerInputComonent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	ConstructorHelpers::FObjectFinder<UInputAction> MouseInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Look.IA_Look'"));
	if (MouseInput.Succeeded())
	{
		mouseInput = MouseInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> MoveInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Move.IA_Move'"));
	if (MoveInput.Succeeded())
	{
		moveInput = MoveInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> EquipInput1(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Change1.IA_Change1'"));
	if (EquipInput1.Succeeded())
	{
		equipChange1Input = EquipInput1.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> EquipInput2(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Change2.IA_Change2'"));
	if (EquipInput2.Succeeded())
	{
		equipChange2Input = EquipInput2.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> MouseLeftClick(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Fire.IA_Fire'"));
	if (MouseLeftClick.Succeeded())
	{
		mouseLecftClick = MouseLeftClick.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> ReloadingInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Reloading.IA_Reloading'"));
	if (ReloadingInput.Succeeded())
	{
		reloadingInput = ReloadingInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> MouseRightClick(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Aim.IA_Aim'"));
	if (MouseRightClick.Succeeded())
	{
		mouseRightClick = MouseRightClick.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> RunInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Run.IA_Run'"));
	if (RunInput.Succeeded())
	{
		runInput = RunInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> InteractionInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Interaction.IA_Interaction'"));
	if (InteractionInput.Succeeded())
	{
		interactionInput = InteractionInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> ThrowawayInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Throwaway.IA_Throwaway'"));
	if (ThrowawayInput.Succeeded())
	{
		throwawayInput = ThrowawayInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> InventoryInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Inventory.IA_Inventory'"));
	if (InventoryInput.Succeeded())
	{
		inventoryInput = InventoryInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> BuildInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Build.IA_Build'"));
	if (BuildInput.Succeeded())
	{
		buildInput = BuildInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> BuildRotateInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_BuildRotate.IA_BuildRotate'"));
	if (BuildRotateInput.Succeeded())
	{
		buildRotateInput = BuildRotateInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> CoverInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Cover.IA_Cover'"));
	if (CoverInput.Succeeded())
	{
		coverInput = CoverInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> CronchInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_Crouch.IA_Crouch'"));
	if (CronchInput.Succeeded())
	{
		cronchInput = CronchInput.Object;
	}
	ConstructorHelpers::FObjectFinder<UInputAction> WeaponMeleeAttackInput(TEXT("EnhancedInput.InputAction'/Game/Input/IA_WeaponMeleeAttack.IA_WeaponMeleeAttack'"));
	if (WeaponMeleeAttackInput.Succeeded())
	{
		weaponMeleeAttackInput = WeaponMeleeAttackInput.Object;
	}
	bWantsInitializeComponent=true;
	// ...
}


// Called when the game starts
void UPlayerInputComonent::BeginPlay()
{
	Super::BeginPlay();
	// ...
	AMainGameModeBase* mainGamemodeBase = Cast<AMainGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
	mainGamemodeBase->OnToggleMouseInput.AddUObject(this, &UPlayerInputComonent::SetIsLookInput);
	mainGamemodeBase->OnToggleMoveInput.AddUObject(this, &UPlayerInputComonent::SetIsMoveInput);
}

void UPlayerInputComonent::InitializeComponent()
{
	Super::InitializeComponent();
	ownerPlayer = Cast<AMainPlayer>(GetOwner());
	if (ownerPlayer)
	{
		ownerPlayer->OnInputBindingDelegate.AddUObject(this,&UPlayerInputComonent::SetUpInputBinding);
		UInteractionComponent* InteractionComp = ownerPlayer->GetCharacterComponent<UInteractionComponent>();
		InteractionComp->OnCanInteraction.AddUObject(this,&UPlayerInputComonent::SetInteractionItem);
		UPlayerMoveComponent* moveComp = ownerPlayer->GetCharacterComponent<UPlayerMoveComponent>();
		moveComp->OnCoverCheck.AddUObject(this,&UPlayerInputComonent::SetIsCover);
		UPlayerAttackComponent* attackComp = ownerPlayer->GetCharacterComponent<UPlayerAttackComponent>();
		attackComp->OnMeleeAttack.AddUObject(this, &UPlayerInputComonent::SetMeleeAttack);
	}

}

void UPlayerInputComonent::SetUpInputBinding(UEnhancedInputComponent* input)
{
	input->BindAction(mouseInput, ETriggerEvent::Triggered, this, &UPlayerInputComonent::Look);
	input->BindAction(moveInput, ETriggerEvent::Started, this, &UPlayerInputComonent::SetControllerRotationYawTrue);
	input->BindAction(moveInput, ETriggerEvent::Triggered, this, &UPlayerInputComonent::Move);
	input->BindAction(moveInput, ETriggerEvent::Completed, this, &UPlayerInputComonent::SetControllerRotationYawFalse);
	input->BindAction(equipChange1Input, ETriggerEvent::Started, this, &UPlayerInputComonent::EquipInputMain);
	input->BindAction(equipChange2Input, ETriggerEvent::Started, this, &UPlayerInputComonent::EquipInputSecondary);
	input->BindAction(mouseLecftClick, ETriggerEvent::Triggered, this, &UPlayerInputComonent::OnMouseLeftClickPressed);
	input->BindAction(mouseLecftClick, ETriggerEvent::Completed, this, &UPlayerInputComonent::OnMouseLeftClickReleased);
	input->BindAction(mouseRightClick, ETriggerEvent::Started, this, &UPlayerInputComonent::OnMouseRightClickPressed);
	input->BindAction(mouseRightClick, ETriggerEvent::Completed, this, &UPlayerInputComonent::OnMouseRightClickReleased);
	input->BindAction(reloadingInput, ETriggerEvent::Started, this, &UPlayerInputComonent::OnReloadingClick);
	input->BindAction(runInput, ETriggerEvent::Started, this, &UPlayerInputComonent::RunStart);
	input->BindAction(runInput, ETriggerEvent::Completed, this, &UPlayerInputComonent::RunRelase);
	input->BindAction(interactionInput, ETriggerEvent::Started, this, &UPlayerInputComonent::InteractionItem);
	input->BindAction(throwawayInput, ETriggerEvent::Started, this, &UPlayerInputComonent::OnThrowawayChange);
	input->BindAction(inventoryInput, ETriggerEvent::Started, this, &UPlayerInputComonent::OnTogglrInventory);
	input->BindAction(buildInput, ETriggerEvent::Started, this, &UPlayerInputComonent::BuildStart);
	input->BindAction(buildRotateInput, ETriggerEvent::Started, this, &UPlayerInputComonent::BuildRotate);
	input->BindAction(buildRotateInput, ETriggerEvent::Completed, this, &UPlayerInputComonent::BuildRotate);
	input->BindAction(coverInput, ETriggerEvent::Started, this, &UPlayerInputComonent::OnCoverCheck);
	input->BindAction(cronchInput, ETriggerEvent::Started, this, &UPlayerInputComonent::SetIsCronch);
	input->BindAction(weaponMeleeAttackInput, ETriggerEvent::Started, this, &UPlayerInputComonent::OnWeaponMeleeAttack);
}
void UPlayerInputComonent::Look(const FInputActionValue& value)
{
	if(!isCanLook)
		return;
	FVector2D LookVector = value.Get<FVector2D>();
	ownerPlayer->AddControllerYawInput(LookVector.X);
	ownerPlayer->AddControllerPitchInput(LookVector.Y);
}

void UPlayerInputComonent::Move(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	if(!isCanMove)
		return;
	FVector2D MoveVector = value.Get<FVector2D>();
	OnMoveCharacter.Broadcast(MoveVector);
}

void UPlayerInputComonent::RunStart(const FInputActionValue& value)
{
	UPlayerStateComponent* stateComp = ownerPlayer->GetCharacterComponent<UPlayerStateComponent>();
	stateComp->SetIsRun(true);
}

void UPlayerInputComonent::RunRelase(const FInputActionValue& value)
{
	UPlayerStateComponent* stateComp = ownerPlayer->GetCharacterComponent<UPlayerStateComponent>();
	stateComp->SetIsRun(false);
}

void UPlayerInputComonent::BuildStart(const FInputActionValue& value)
{
	if(isAim)
		return;
	if(!isBuildMode)
	{
		OnToggleBuild.Broadcast(true);
		isBuildMode=true;
	}
	else
	{
		OnToggleBuild.Broadcast(false);
		isBuildMode=false;
	}
}

void UPlayerInputComonent::EquipInputMain(const FInputActionValue& value)
{
	EquipChange(EWeaponSlot::Main);
}
void UPlayerInputComonent::EquipInputSecondary(const FInputActionValue& value)
{
	EquipChange(EWeaponSlot::Secondary);
}
void UPlayerInputComonent::OnThrowawayChange(const FInputActionValue& value)
{
	EquipChange(EWeaponSlot::Throwaway);
}
void UPlayerInputComonent::OnCoverCheck(const FInputActionValue& value)
{
	if (!coverToggle)
	{
		coverToggle =true;
		OnCover.Broadcast(coverToggle);
		return;
	}
	coverToggle = false;
	OnCover.Broadcast(coverToggle);
}
void UPlayerInputComonent::OnWeaponMeleeAttack(const FInputActionValue& value)
{
	if(isCronch||isCoverCheck)
		return;
	OnMeleeAttack.Broadcast();
}
void UPlayerInputComonent::EquipChange(EWeaponSlot weaponslot)
{
	if (isMeleeAttack)
		return;
	OnEquipChange.Broadcast(weaponslot);
}

void UPlayerInputComonent::BuildRotate(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	if(!isBuildMode)
		return;
	if (!isBuildRotate)
	{
		OnBuildRotateActor.Execute(true);
		isBuildRotate=true;
		return;
	}
	OnBuildRotateActor.Execute(false);
	isBuildRotate = false;
}

void UPlayerInputComonent::OnTogglrInventory(const FInputActionValue& value)
{
	if(isInventoryWidgetOpen)
	{
		OnToggleInventoryWidget.Broadcast(false);
		isInventoryWidgetOpen=false;
	}
	else
		OnToggleInventoryWidget.Broadcast(true);
	ownerPlayer->TogglePlayerInventory();
}

void UPlayerInputComonent::OnMouseLeftClickReleased(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	if(isCanLook)
		OnAttackInput.Execute(false);
}
void UPlayerInputComonent::OnMouseLeftClickPressed(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	if (isCanLook)
		OnAttackInput.Execute(true);
	if (isBuildMode)
	{
		OnBuildActor.Execute();
		isBuildMode=false;
	}
}
void UPlayerInputComonent::OnMouseRightClickPressed(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	if (!isCanLook|| isBuildMode)
		return;
	if (isCoverCheck)
	{
		FHitResult hitResult;
		FCollisionQueryParams params;
		ECollisionChannel channel = ECC_GameTraceChannel6;
		FVector posTemp=FVector(0,0,60);
		FVector ownerPos = GetOwner()->GetActorLocation();
		FVector forwardvector = ownerPlayer->GetFireEndCheckVector(500);
		bool isCoverHit = GetWorld()->LineTraceSingleByChannel(hitResult, ownerPos+ posTemp, forwardvector, channel, params);
		if(isCoverHit)
			return;
	}
	isAim=true;
	OnMouseAim.Broadcast(true);
	ownerPlayer->bUseControllerRotationYaw = true;
}

void UPlayerInputComonent::OnMouseRightClickReleased(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	if (!isCanLook|| !isAim)
		return;
	isAim=false;
	OnMouseAim.Broadcast(false);
	if(!isMove)
	ownerPlayer->bUseControllerRotationYaw = false;
}
void UPlayerInputComonent::OnReloadingClick(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	if (!isCanLook)
		return;
	OnReloading.Broadcast();
}

void UPlayerInputComonent::SetControllerRotationYawTrue(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	isMove=true;
	if(!isCoverCheck)
		ownerPlayer->bUseControllerRotationYaw = true;
}

void UPlayerInputComonent::SetControllerRotationYawFalse(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	isMove=false;
	if(!isAim)//|| !isCoverCheck)
	ownerPlayer->bUseControllerRotationYaw = false;
}

void UPlayerInputComonent::InteractionItem(const FInputActionValue& value)
{
	if (isMeleeAttack)
		return;
	if(isBuildMode)
		return;
	if (isInteraction)
	{
		OnInteraction.Execute();
	}
}

void UPlayerInputComonent::SetInteractionItem(bool value)
{
	isInteraction=value;
}

void UPlayerInputComonent::SetIsLookInput(bool value)
{
	isCanLook=value;
}

void UPlayerInputComonent::SetIsMoveInput(bool value)
{
	isCanMove=value;
}

void UPlayerInputComonent::SetIsCover(bool value)
{
	if(isMeleeAttack)
		return;
	isCoverCheck=value;
	if(isCoverCheck)
	{
		ownerPlayer->bUseControllerRotationYaw = false;
		return;
	}
	isCronch = false;
}

void UPlayerInputComonent::SetMeleeAttack(bool value)
{
	isMeleeAttack=value;
	ownerPlayer->bUseControllerRotationYaw = false;
}

void UPlayerInputComonent::SetIsCronch(const FInputActionValue& value)
{
	if(isCoverCheck|| isMeleeAttack)
		return;
	if (isCronch)
	{
		isCronch=false;
		OnCronuch.Broadcast(false);
		return;
	}
	isCronch=true;
	OnCronuch.Broadcast(true);
}




