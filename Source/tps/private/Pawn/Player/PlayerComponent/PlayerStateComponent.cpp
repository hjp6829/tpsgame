// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/PlayerComponent/PlayerStateComponent.h"
#include "Pawn/Player/MainPlayer.h"
#include "Pawn/Player/PlayerComponent/PlayerMoveComponent.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/SubSystem/DataManagerSubsystem.h"
void UPlayerStateComponent::BeginPlay()
{
	Super::BeginPlay();
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UDataManagerSubsystem* dataManager = gameInstance->GetSubsystem<UDataManagerSubsystem>();
	if (dataManager->CheckContainDB("Player"))
	{
		TMap<int, TMap<FString, FVariant>>& dataTemp = dataManager->GetTableData("Player");
		TMap<FString, FVariant>& data = dataTemp[0];
		maxHp = data["MaxHP"].GetValue<int32>();
		walkSpeed = data["WalkSpeed"].GetValue<int32>();
		runSpeed = data["RunSpeed"].GetValue<int32>();
		gold = data["Gold"].GetValue<int32>();
	}
	AMainPlayer* ownerPlayer = Cast<AMainPlayer>(GetOwner());
	UPlayerMoveComponent* moveComp = ownerPlayer->GetCharacterComponent<UPlayerMoveComponent>();
	moveComp->SetRunAndWalkSpeed(walkSpeed, runSpeed);
	currentHP = maxHp;
	currentHP-=60;
	UPlayerInputComonent* inputComp = ownerPlayer->GetCharacterComponent<UPlayerInputComonent>();
	inputComp->OnMouseAim.AddUObject(this, &UPlayerStateComponent::SetIsAim);
	OnHPUpdate.Broadcast(maxHp,currentHP);
}

UPlayerStateComponent::UPlayerStateComponent()
{
	//bWantsInitializeComponent = true;
}

void UPlayerStateComponent::UseGold(int32 value)
{
	gold-=value;
}

void UPlayerStateComponent::InitializeComponent()
{
UE_LOG(LogTemp,Warning,TEXT("asd"));

}
