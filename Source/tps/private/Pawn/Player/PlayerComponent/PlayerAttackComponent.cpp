// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/PlayerComponent/PlayerAttackComponent.h"
#include "Pawn/Player/MainPlayer.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
#include "Engine/GameInstance.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Managers/WeaponFactory.h"
#include "Weapon/WeaponBase.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Pawn/Player/Animation/PlayerAnimInstance.h"
// Sets default values for this component's properties
UPlayerAttackComponent::UPlayerAttackComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent=true;
	// ...
}


// Called when the game starts
void UPlayerAttackComponent::BeginPlay()
{
	Super::BeginPlay();
}
void UPlayerAttackComponent::InitializeComponent()
{
	Super::InitializeComponent();
	ACustomCharacter* player = Cast<ACustomCharacter>(GetOwner());
	UPlayerInputComonent* playerInput = player->GetCharacterComponent<UPlayerInputComonent>();
	if (playerInput)
	{
		playerInput->OnAttackInput.BindUObject(this, &UPlayerAttackComponent::SetWeaponState);
		playerInput->OnMouseAim.AddUObject(this, &UPlayerAttackComponent::SetNowAim);
		playerInput->OnReloading.AddUObject(this,&UPlayerAttackComponent::ReloadWeapon);
		playerInput->OnToggleInventoryWidget.AddUObject(this,&UPlayerAttackComponent::UpdateBulletCountInInventory);
		playerInput->OnMeleeAttack.AddUObject(this, &UPlayerAttackComponent::SetMeleeAttackMontage);
	}
	UAnimInstance* animInstance = owerCharacter->GetMesh()->GetAnimInstance();
	UPlayerAnimInstance* playerAnimInstance = Cast<UPlayerAnimInstance>(animInstance);
	playerAnimInstance->OnMeleeAttackCollision.AddUObject(this,&UPlayerAttackComponent::MakeWeaponMeleeCollider);
}

void UPlayerAttackComponent::UpdateBulletCountInInventory(bool value)
{
	if(!value)
		return;
	ARangedWeaponBase* rangeweapon = Cast<ARangedWeaponBase>(CurrentWeapon);
	if (rangeweapon)
	{
		if(rangeweapon->CheckDefaultBulletLeft())
			return;
		int32 count = rangeweapon->GetUsedBulletConut();
		int32 bulletIDX= rangeweapon->GetCurrentBulletIDX();
		ACustomCharacter* player = Cast<ACustomCharacter>(GetOwner());
		UPlayerInventoryComoponent* inventoryComp = player->GetCharacterComponent<UPlayerInventoryComoponent>();
		inventoryComp->UseItem(bulletIDX, count);
		rangeweapon->ResetUsedBullrtCount();
	}
}


