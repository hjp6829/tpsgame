// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/PlayerComponent/BuildSystemComponent.h"
#include "Pawn/Player/MainPlayer.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
// Sets default values for this component's properties
UBuildSystemComponent::UBuildSystemComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBuildSystemComponent::BeginPlay()
{
	Super::BeginPlay();
	player=Cast<AMainPlayer>(GetOwner());
	UPlayerInputComonent* inputComp=player->GetCharacterComponent<UPlayerInputComonent>();
	inputComp->OnToggleBuild.AddUObject(this,&UBuildSystemComponent::SetPreviewActor);
	inputComp->OnBuildActor.BindUObject(this,&UBuildSystemComponent::SetUpBuildActor);
	inputComp->OnBuildRotateActor.BindUObject(this, &UBuildSystemComponent::SetBuildPreviewRotate);
	// ...
	
}


// Called every frame
void UBuildSystemComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	if(isBuildMode)
		BuildPreview();
	// ...
}

void UBuildSystemComponent::BuildPreview()
{
	FHitResult hitResult;
	FCollisionQueryParams params;
	ECollisionChannel channel = ECC_GameTraceChannel4;
	
	bool hit = GetWorld()->LineTraceSingleByChannel(hitResult, player->GetFireEndCheckStartPos(), player->GetFireEndCheckVector(10000), channel, params);
	if(!hit)
		return;
	if(previewActor!=nullptr)
		previewActor->SetActorLocation(hitResult.ImpactPoint);
}

void UBuildSystemComponent::SetUpBuildActor()
{
	previewActor=nullptr;
	SetPreviewActor(false);
}

void UBuildSystemComponent::SetBuildPreviewRotate(bool value)
{
	if(value)
		GetWorld()->GetTimerManager().SetTimer(rotationHandle,this,&UBuildSystemComponent::SetPreviewRotation,0.1f,true);
	else
		GetWorld()->GetTimerManager().ClearTimer(rotationHandle);
}

void UBuildSystemComponent::SetPreviewRotation()
{
	FRotator previewRotation = previewActor->GetActorRotation();
	FRotator rotationValue = FRotator(0, 10, 0);
	previewRotation += rotationValue;
	previewActor->SetActorRotation(previewRotation);
}

void UBuildSystemComponent::SetPreviewActor(bool value)
{
	isBuildMode =value;
	if (!value)
	{
		if (previewActor)
		{
			GetWorld()->DestroyActor(previewActor);
			previewActor=nullptr;
		}
		return;
	}
	if (previewActor==nullptr)
	{
		previewActor = GetWorld()->SpawnActor<AActor>(previewActorClass,FVector::Zero(), FRotator::ZeroRotator);
	}
}

