// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Pawn/Player/MainPlayer.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
#include "Weapon/WeaponBase.h"
#include "Pawn/Player/PlayerComponent/PlayerAttackComponent.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Engine/GameInstance.h"
#include "UI/Player/PlayerHUD.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "Managers/GameItemManager.h"
#include "Item/HillItem.h"
#include "Item/EquipItem.h"
#include "Interfaces/Interfaces.h"
TMap<EWeaponSlot, class AWeaponBase*>& UPlayerInventoryComoponent::GetEquipWeaponMap()
{
	return EquipedWeapon;
}
void UPlayerInventoryComoponent::InitializeComponent()
{
	Super::InitializeComponent();
	AMainPlayer* player = Cast<AMainPlayer>(GetOwner());
	UPlayerInputComonent* playerInput = player->GetCharacterComponent<UPlayerInputComonent>();
	if (playerInput)
	{
		playerInput->OnEquipChange.AddUObject(this, &UPlayerInventoryComoponent::ChangeEquip);
	}
}
void UPlayerInventoryComoponent::BeginPlay()
{
	Super::BeginPlay();
	FTimerHandle UnusedHandle;
	GetWorld()->GetTimerManager().SetTimer(UnusedHandle, this, &UPlayerInventoryComoponent::Initialize, 0.5f, false);
	OnInventoryUpdate.AddUObject(this,&UPlayerInventoryComoponent::UpdateWeaponMagazine);
}
void UPlayerInventoryComoponent::ChangeEquip(EWeaponSlot weaponslot)
{
	if (currentWeaponSlot == weaponslot)
		return;
	AWeaponBase* newCurrentWeapon = EquipedWeapon[weaponslot];
	if(!IsValid(newCurrentWeapon))
		return;
	if(weaponslot== EWeaponSlot::Throwaway&&ThrowWeaponCount==0)
		return;
	currentWeaponSlot = weaponslot;
	AMainPlayer* player = Cast<AMainPlayer>(GetOwner());
	newCurrentWeapon->SetOwner(player);

	newCurrentWeapon->OnAttack.AddUObject(this,&UPlayerInventoryComoponent::TestFuntion);
	newCurrentWeapon->OnAmmoReloaded.AddUObject(this, &UPlayerInventoryComoponent::ReloadBulletUpdate);
	playerHUD=player->GetPlayerHUD();
	ARangedWeaponBase* range = Cast<ARangedWeaponBase>(newCurrentWeapon);
	int32 weaponBulletIDX=range->GetCurrentBulletIDX();
	int32 inventoryBulletCount= GetItemNum(weaponBulletIDX);
	range->SetInventoryBullet(inventoryBulletCount);
	playerHUD->SetEquipWeaponBullet(inventoryBulletCount, range->GetMaxMagazine());

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	range->AttachToComponent(player->GetMesh(), AttachmentRules, "WeaponSoket");
	UPlayerAttackComponent* playerAttackComp = player->GetCharacterComponent<UPlayerAttackComponent>();
	if (playerAttackComp)
	{
		AWeaponBase* currentWeapon = playerAttackComp->GetCurrentWeapon();
		playerAttackComp->SetCurrentWeapon(range);
		if (!currentWeapon)
			return;
		currentWeapon->ResetWeapon();
	}
}

void UPlayerInventoryComoponent::SetWeaponEquip(EWeaponSlot weaponslot, EWeaponType weapontype)
{
	UGameInstance* gameinstance = Owner->GetGameInstance<UGameInstance>();
	UServiceLocatorServeSystem* serviceLocator = gameinstance->GetSubsystem<UServiceLocatorServeSystem>();
 	AGameItemManager* itemManager = serviceLocator->GetService<AGameItemManager>();
	AWeaponBase* weapon = itemManager->GetWeaponFromPool<AWeaponBase>(weapontype);
	EquipedWeapon[weaponslot] = weapon;
}

void UPlayerInventoryComoponent::TestFuntion(int32 idx, int32 value)
{
	///hud같은곳에 총알을 몇개 소모했는지 전해줌
	playerHUD->UpdateCachedBullet(value);
}

void UPlayerInventoryComoponent::ReloadBulletUpdate(int32 idx, int32 value)
{
	///여기서 hud에 장전했을때 얼마나 장전했는지 보여줘야함
	playerHUD->UpdateReloadBullet(value);
}

void UPlayerInventoryComoponent::Initialize()
{
	SetWeaponEquip(EWeaponSlot::Main,  EWeaponType::Rifle);
	SetWeaponEquip(EWeaponSlot::Secondary, EWeaponType::Shotgun);
	SetWeaponEquip(EWeaponSlot::Throwaway, EWeaponType::Grenade);
	ChangeEquip(EWeaponSlot::Main);	
}

void UPlayerInventoryComoponent::UpdateWeaponMagazine()
{
	AMainPlayer* player = Cast<AMainPlayer>(GetOwner());
	UPlayerAttackComponent* playerAttackComp = player->GetCharacterComponent<UPlayerAttackComponent>();
	AWeaponBase* currentWeapon= playerAttackComp->GetCurrentWeapon();
	ARangedWeaponBase* RangeWeaponTemp= Cast<ARangedWeaponBase>(currentWeapon);
	int32 inventoryBulletCount = GetItemNum(RangeWeaponTemp->GetCurrentBulletIDX());
	RangeWeaponTemp->SetInventoryBullet(inventoryBulletCount);
	playerHUD->SetEquipWeaponBullet(inventoryBulletCount, RangeWeaponTemp->GetAmmunition());
}

void UPlayerInventoryComoponent::SetWeaponEquip(EWeaponSlot WeaponSlot, AWeaponBase* newWeapon)
{	
	EquipedWeapon[WeaponSlot] = newWeapon;
}

void UPlayerInventoryComoponent::AddItem(UItemBase* item)
{
	Super::AddItem(item);
	//ThrowWeaponCount=0;
	//for (int32 i = 0; i < 20; i++)
	//{
	//	if(inventory[i]==nullptr)
	//	continue;
	//	if (inventory[i]->item->GetItemType() == EWeaponType::Grenade)
	//	{
	//		ThrowWeaponCount += inventory[i]->item->GetNowItemNumber();
	//	}
	//}
	//if (ThrowWeaponCount != 0)
	//	playerHUD->UpdateThrowWeaponNum(ThrowWeaponCount);
}
