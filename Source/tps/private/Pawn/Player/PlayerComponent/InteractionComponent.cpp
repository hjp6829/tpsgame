// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Player/PlayerComponent/InteractionComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Interfaces/Interfaces.h"
#include "Pawn/Player/MainPlayer.h"
#include "Pawn/Player/PlayerComponent/PlayerInputComonent.h"
// Sets default values for this component's properties
UInteractionComponent::UInteractionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent=true;
	// ...
}


// Called when the game starts
void UInteractionComponent::BeginPlay()
{
	Super::BeginPlay();
	ObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_GameTraceChannel2));
	// ...
	
}

void UInteractionComponent::InitializeComponent()
{
	Super::InitializeComponent();
	AMainPlayer* ownerPlayer = Cast<AMainPlayer>(GetOwner());
	if (ownerPlayer)
	{
		UPlayerInputComonent* InputComp = ownerPlayer->GetCharacterComponent<UPlayerInputComonent>();
		InputComp->OnInteraction.BindUObject(this,&UInteractionComponent::InteractionObject);
	}
}
// Called every frame
void UInteractionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	TArray<AActor*> ActorsToIgnore;
	TArray<AActor*> OverlappedActors;
	UKismetSystemLibrary::SphereOverlapActors(
	GetWorld(),
	Owner->GetActorLocation(),
	500,
		ObjectTypes,
		nullptr,
		ActorsToIgnore,
		OverlappedActors
	);
	if (OverlappedActors.Num() == 0)
	{
		OnCanInteraction.Broadcast(false);
		if (currentInteractActor!=nullptr)
		{
			IInteractable* Interactable = Cast<IInteractable>(currentInteractActor);
			if(Interactable)
				Interactable->InteractEnd();
			currentInteractActor = nullptr;
		}
		return;
	}
	TArray<AActor*> InAngleActors;
	for (AActor* OverlappedActor : OverlappedActors)
	{
		IInteractable* Interactable = Cast<IInteractable>(OverlappedActor);
		if (Interactable)
		{
			InAngleActors.Add(OverlappedActor);
		}
	}
	InAngleActors.Sort();
	FVector targetPos = InAngleActors[0]->GetActorLocation();
	FVector direction = (targetPos - GetOwner()->GetActorLocation()).GetSafeNormal();
	float targetAngle = FMath::Acos(FVector::DotProduct(GetOwner()->GetActorForwardVector(), direction));
	targetAngle = FMath::RadiansToDegrees(targetAngle);
	if (targetAngle <= ViewAngle * 0.5f)
		OnCanInteraction.Broadcast(true);
	currentInteractActor = InAngleActors[0];
	// ...
}

void UInteractionComponent::InteractionObject()
{
	IInteractable* Interactable = Cast<IInteractable>(currentInteractActor);
	Interactable->Interact(Cast<AMainPlayer>(GetOwner()));
}

