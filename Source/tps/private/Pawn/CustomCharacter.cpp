// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/CustomCharacter.h"

// Sets default values
ACustomCharacter::ACustomCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACustomCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACustomCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	float pitchTemp = GetController()->GetControlRotation().Pitch;
	if (pitchTemp >= 270)
		pitchTemp -= 360;
	float AimYaw = GetController()->GetControlRotation().Yaw;// - GetActorRotation().Yaw;
	AimYaw = FMath::UnwindDegrees(AimYaw);
	OnSetCharacterYawAndPitch.Broadcast(AimYaw, pitchTemp);
}

// Called to bind functionality to input
void ACustomCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

