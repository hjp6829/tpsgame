// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Components/Base/AttackComponent.h"
#include "Pawn/CustomCharacter.h"
#include "Weapon/WeaponBase.h"
#include "Pawn/Player/PlayerComponent/PlayerMoveComponent.h"
#include "Kismet/GameplayStatics.h"
// Sets default values for this component's properties
UAttackComponent::UAttackComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UAttackComponent::InitializeComponent()
{
	Super::InitializeComponent();
	owerCharacter = Cast<ACustomCharacter>(GetOwner());
	owerCharacter->OnSetCharacterYawAndPitch.AddUObject(this, &UAttackComponent::SetPlayerYawAndPitch);
}

void UAttackComponent::SetPlayerYawAndPitch(float yaw, float pitch)
{
	ownerYaw = yaw;
	ownerPitch = pitch;
}

void UAttackComponent::SetMeleeAttackMontage()
{
	UAnimInstance* animInstance = owerCharacter->GetMesh()->GetAnimInstance();
	UAnimMontage* montageTemp = CurrentWeapon->GetMeleeMontage();
	FOnMontageEnded montageEndedDelegate;
	montageEndedDelegate.BindUObject(this, &UAttackComponent::EndMeleeAttackMontage);
	animInstance->Montage_Play(montageTemp);
	animInstance->Montage_SetEndDelegate(montageEndedDelegate, montageTemp);
	OnMeleeAttack.Broadcast(true);
}

void UAttackComponent::EndMeleeAttackMontage(UAnimMontage* Montage, bool bInterrupted)
{
	OnMeleeAttack.Broadcast(false);
}

void UAttackComponent::MakeWeaponMeleeCollider(bool value)
{
	FVector startLocation= owerCharacter->GetMeleeAttackLocation();
	FVector endLocation= startLocation+ owerCharacter->GetActorForwardVector()*50;
	FVector boxSize=FVector(50,50,50);
	FQuat Orientation = FQuat::FindBetweenNormals(FVector::ForwardVector, owerCharacter->GetActorForwardVector());
	TArray<FHitResult> OutHits;
	ECollisionChannel TraceChannel = ECC_GameTraceChannel1;
	FCollisionQueryParams Params;
	bool hit = GetWorld()->SweepMultiByChannel(OutHits, startLocation, endLocation, Orientation, TraceChannel, FCollisionShape::MakeBox(boxSize), Params);
	if (hit)
	{
		for (const FHitResult hitresult : OutHits)
		{
			TSubclassOf<UDamageType> damageTypeClass = UDamageType::StaticClass();
			UGameplayStatics::ApplyDamage(
				hitresult.GetActor(),
				20,
				owerCharacter->GetController(),
				owerCharacter,
				damageTypeClass
			);
		}
	}
	FVector Center = (startLocation + endLocation) / 2.0f;
	FVector BoxExtent = (endLocation - startLocation) / 2.0f;
	BoxExtent = BoxExtent.ComponentMax(boxSize);
	DrawDebugBox(GetWorld(), Center, BoxExtent, Orientation, FColor::Red, false, 5.0f);
}

// Called when the game starts
void UAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UAttackComponent::SetWeaponState(bool value)
{
	if (isReloading)
		return;
	if (value)
	{
		OnAttackWeaponStart();
	}
	else
	{
		OnAttackWeaponRelease();
	}
}

void UAttackComponent::OnAttackWeaponStart()
{
	if (isAim)
		CurrentWeapon->AttackStart(ownerYaw, ownerPitch);
}

void UAttackComponent::OnAttackWeaponRelease()
{
	if (isAim)
		CurrentWeapon->AttackRelease();
}

void UAttackComponent::ReloadWeapon()
{
	if (!CurrentWeapon->CheckCanReload())
		return;
	CurrentWeapon->ReloadWeapon();
	isReloading = true;
	GetWorld()->GetTimerManager().SetTimer(OnReloadTimer, this, &UAttackComponent::WeaponReloadEnd, CurrentWeapon->GetReloadTime(), false);
}

void UAttackComponent::WeaponReloadEnd()
{
	isReloading = false;
}
AWeaponBase* UAttackComponent::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void UAttackComponent::SetCurrentWeapon(AWeaponBase* weapon)
{
	CurrentWeapon = weapon;
}
void UAttackComponent::SetNowAim(bool value)
{
	isAim = value;
}

bool UAttackComponent::NowReloadingCheck()
{
	return isReloading;
}
