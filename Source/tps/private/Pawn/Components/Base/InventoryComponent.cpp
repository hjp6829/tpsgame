// Fill out your copyright notice in the Description page of Project Settings.


#include "Pawn/Components/Base/InventoryComponent.h"
#include "Components/SceneComponent.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Managers/GameItemManager.h"
#include "Kismet/GameplayStatics.h"
#include "Item/HillItem.h"
#include "Item/AmmunitionItem.h"
#include "Item/ItemFactory.h"
// Sets default values for this component's properties
UInventoryComponent::UInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent=true;
	// ...
}


// Called when the game starts
void UInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
	EquipedWeapon.Add(EWeaponSlot::Main, nullptr);
	EquipedWeapon.Add(EWeaponSlot::Secondary, nullptr);
	EquipedWeapon.Add(EWeaponSlot::Throwaway, nullptr);
	currentWeaponSlot = EWeaponSlot::NONE;
	inventory.SetNum(20);
	// ...
	
}

void UInventoryComponent::AddItem(class UItemBase* itembase)
{
		FInventorySlot** foundItem = inventory.FindByPredicate([itembase](const FInventorySlot* slot)
			{
				return slot && slot->GetItemIDX() == itembase->GetIDX();
			});
		if (foundItem)
		{
			int32 maxOverlap = (*foundItem)->GetMaxOverlapNumber();
			if ((*foundItem)->item->GetNowItemNumber() + itembase->GetNowItemNumber() <= maxOverlap)
			{
				(*foundItem)->item->SetNowItemNumber((*foundItem)->item->GetNowItemNumber() + itembase->GetNowItemNumber());
			}
			else
			{
				int32 leftNum = (*foundItem)->item->GetNowItemNumber() + itembase->GetNowItemNumber() - (*foundItem)->GetMaxOverlapNumber();
				(*foundItem)->item->SetNowItemNumber((*foundItem)->GetMaxOverlapNumber());
				AddNewItem(itembase->GetIDX(), leftNum);
			}
			if (OnInventoryUpdate.IsBound())
				OnInventoryUpdate.Broadcast();
			return;
		}
	AddNewItem(itembase->GetIDX(), itembase->GetNowItemNumber());
}

TArray<FInventorySlot*>& UInventoryComponent::GetInventory()
{
	return inventory;
}

void UInventoryComponent::UseItem(int32 idx,int32 count)
{
	for (int32 i = inventory.Num() - 1; i >= 0; i--)
	{
		if(inventory[i]==nullptr|| inventory[i]->GetItemIDX() != idx)
			continue;
		if (inventory[i]->item->GetNowItemNumber() - count >= 0)
		{
			inventory[i]->item->SetNowItemNumber(inventory[i]->item->GetNowItemNumber()- count);
			if (inventory[i]->item->GetNowItemNumber() == 0)
			{
				inventory[i] = nullptr;
			}
		}
		else
		{
			int32 leftCount= count- inventory[i]->item->GetNowItemNumber();
			inventory[i]=nullptr;
			UseItem(idx, count);
		}
		break;
	}
	if (OnInventoryUpdate.IsBound())
		OnInventoryUpdate.Broadcast();
}

int32 UInventoryComponent::GetItemNum(int32 idx)
{
	int value=0;
	for (int i = inventory.Num() - 1; i >= 0; i--)
	{
		if (inventory[i] == nullptr)
			continue;
		if (inventory[i]->GetItemIDX() == idx)
		{
			value+=inventory[i]->item->GetNowItemNumber();
		}
	}
	return value;
}

TMap<EWeaponSlot, class AWeaponBase*>& UInventoryComponent::GetWeaponSlot()
{
	return EquipedWeapon;
}

void UInventoryComponent::SetNULLIDX(int32 idx)
{
	inventory[idx]=nullptr;
	if (OnInventoryUpdate.IsBound())
		OnInventoryUpdate.Broadcast();
}

void UInventoryComponent::AddNewItem(int32 itemIDX, int32 num)
{
	int32 remainingNumber = 0;
	for (int32 i = 0; i < 20; i++)
	{
		if (inventory[i] == nullptr)
		{
			FInventorySlot* newSlot = new FInventorySlot();
			UItemBase* item = ItemFactory::CreateItem(itemIDX, num, GetWorld());
			newSlot->item = item;
			if (num > item->getMaxOverlapNumber())
			{
				remainingNumber = num - item->getMaxOverlapNumber();
				item->SetNowItemNumber(item->getMaxOverlapNumber());
			}
			else
				item->SetNowItemNumber(num);
			newSlot->inventoryIDX = i;
			inventory[i] = newSlot;
			break;
		}
	}
	if (remainingNumber != 0)
		AddNewItem(itemIDX, remainingNumber);
	if (OnInventoryUpdate.IsBound())
		OnInventoryUpdate.Broadcast();
}

