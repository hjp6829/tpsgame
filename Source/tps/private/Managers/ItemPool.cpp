// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/ItemPool.h"

// Sets default values
AItemPool::AItemPool()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}
void AItemPool::InPoolItem(ASpawnItemBase* item)
{
	item->EnableActor();
	actorQueue.Enqueue(item);
}
ASpawnItemBase* AItemPool::GetPoolItem()
{
	ASpawnItemBase* temp = nullptr;
	actorQueue.Dequeue(temp);
	temp->ableActor();
	return temp;
}
// Called when the game starts or when spawned
void AItemPool::BeginPlay()
{
	Super::BeginPlay();
	for (int32 i = 0; i < spawnNum; i++)
	{
		ASpawnItemBase* temp = GetWorld()->SpawnActor<ASpawnItemBase>(itemClass,FVector(-240,40,-470), FRotator::ZeroRotator);
		actorQueue.Enqueue(temp);
	}
}

