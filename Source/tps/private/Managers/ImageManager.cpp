// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/ImageManager.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Texture2D.h"
UTexture2D* AImageManager::GetItemImage(int32 idx)
{
	return itemImageMap[idx];
}
void AImageManager::BeginPlay()
{
	Super::BeginPlay();
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* serviceLocatorSystem = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	serviceLocatorSystem->Register(this);
}
