// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/WeaponFactory.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "Weapon/MeleeWeapons/MeleeWaeponBase.h"
#include "Engine/GameInstance.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
// Sets default values
AWeaponFactory::AWeaponFactory()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

AWeaponBase* AWeaponFactory::GetWeapon(EAttackType attackType, EWeaponType weaponType)
{
	switch (attackType)
	{
	case EAttackType::RangeWeapon:
		return RangeWeapons[weaponType];
	case EAttackType::MeleeWeapon:
		return MeleeWeapons[weaponType];
	}
	return nullptr;
}

// Called when the game starts or when spawned
void AWeaponFactory::BeginPlay()
{
	Super::BeginPlay();
	UGameInstance* gameinstance = GetGameInstance<UGameInstance>();
	UServiceLocatorServeSystem* servicelocator = gameinstance->GetSubsystem<UServiceLocatorServeSystem>(gameinstance);
	servicelocator->Register(this);
	for (int32 i = 0; i < RangeWeaponClass.Num(); i++)
	{
		ARangedWeaponBase* rangeWeapon = GetWorld()->SpawnActor<ARangedWeaponBase>(RangeWeaponClass[i], FVector::ZeroVector, FRotator::ZeroRotator);
		RangeWeapons.Add(rangeWeapon->GetWeaponType(),rangeWeapon);
	}
	for (int32 i = 0; i < MeleeWeaponClass.Num(); i++)
	{
		AMeleeWaeponBase* meleeWeapon = GetWorld()->SpawnActor<AMeleeWaeponBase>(MeleeWeaponClass[i], FVector::ZeroVector, FRotator::ZeroRotator);
		MeleeWeapons.Add(meleeWeapon->GetWeaponType(),meleeWeapon);
	}
}



