// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/GameItemManager.h"
#include "Kismet/GameplayStatics.h"
#include "Item/SpawnItemBase.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
// Sets default values
AGameItemManager::AGameItemManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}
void AGameItemManager::InItemToPool(ASpawnItemBase* item, EPoolType type)
{
	if (itemPoolList.Contains(type))
		itemPoolList[type]->InPoolItem(item);
	else
		UE_LOG(LogTemp, Warning, TEXT("PoolIsNull"));
}

void AGameItemManager::InWeaponToPool(ASpawnItemBase* item, EWeaponType type)
{
	if (WeaponPoolList.Contains(type))
		WeaponPoolList[type]->InPoolItem(item);
	else
		UE_LOG(LogTemp, Warning, TEXT("WeaponPoolIsNull"));
}

void AGameItemManager::BeginPlay()
{
	Super::BeginPlay();
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* serviceLocatorSystem = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	serviceLocatorSystem->Register(this);
}

