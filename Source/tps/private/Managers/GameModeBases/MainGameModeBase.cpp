// Fill out your copyright notice in the Description page of Project Settings.


#include "Managers/GameModeBases/MainGameModeBase.h"
#include "Blueprint/UserWidget.h"

void AMainGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	
}
void AMainGameModeBase::ToggleMouseInput(bool value)
{
	OnToggleMouseInput.Broadcast(value);
}

void AMainGameModeBase::ToggleMoveInput(bool value)
{
	OnToggleMoveInput.Broadcast(value);
}
