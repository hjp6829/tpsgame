// Fill out your copyright notice in the Description page of Project Settings.


#include "Item/ItemBase.h"

int32 UItemBase::getMaxOverlapNumber()
{
	return maxOverlapNumber;
}

int32 UItemBase::GetIDX()
{
	return itemIDX;
}

EItemType UItemBase::GetItemType()
{
	return itemType;
}

void UItemBase::SetItemData(TMap<FString, TMap<int, TMap<FString, FVariant>>>& data, int32 idx)
{
	itemIDX= idx;
}

FString UItemBase::GetItemPath()
{
	return itemPath;
}

void UItemBase::SetNowItemNumber(int32 num)
{
	nowverlapNumber= num;
}

int32 UItemBase::GetNowItemNumber()
{
	return nowverlapNumber;
}
