// Fill out your copyright notice in the Description page of Project Settings.


#include "Item/SpawnItemBase.h"

// Sets default values
ASpawnItemBase::ASpawnItemBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}
// Called when the game starts or when spawned
void ASpawnItemBase::BeginPlay()
{
	Super::BeginPlay();
	
}

