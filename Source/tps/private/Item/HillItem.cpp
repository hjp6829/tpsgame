// Fill out your copyright notice in the Description page of Project Settings.


#include "Item/HillItem.h"
#include "Pawn/Player/PlayerComponent/PlayerStateComponent.h"
#include "Pawn/CustomCharacter.h"

void UHillItem::ItemAction(ACustomCharacter* character)
{
	Super::ItemAction(character);
	UPlayerStateComponent* stateComp = character->GetCharacterComponent<UPlayerStateComponent>();
	stateComp->HillCharacter(hillAmount);
}

void UHillItem::SetItemData(TMap<FString, TMap<int, TMap<FString, FVariant>>>& data, int32 idx)
{
	Super::SetItemData(data,idx);
	TMap<int, TMap<FString, FVariant>>& dataTemp = data["ItemBase"];
	TMap<FString, FVariant>& itemdata = dataTemp[idx];
	itemName = itemdata["ItemName"].GetValue<FString>();
	maxOverlapNumber = itemdata["ItemOverlap"].GetValue<int32>();
	itemPrice = itemdata["ItemPrice"].GetValue<int32>();
	itemPath= itemdata["MeshPath"].GetValue<FString>();
	FString ForeignKey = itemdata["ForeignKey"].GetValue<FString>();
	if (data.Contains(ForeignKey))
	{
		TMap<int, TMap<FString, FVariant>>& foreignDataTemp = data[ForeignKey];
		TMap<FString, FVariant>& foreignData = foreignDataTemp[idx];
		hillAmount = foreignData["HillAmount"].GetValue<int32>();
	}
	itemType= EItemType::Hill;

}
