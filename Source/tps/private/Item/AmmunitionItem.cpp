// Fill out your copyright notice in the Description page of Project Settings.


#include "Item/AmmunitionItem.h"

void UAmmunitionItem::SetItemData(TMap<FString, TMap<int, TMap<FString, FVariant>>>& data, int32 idx)
{
	Super::SetItemData(data, idx);
	TMap<int, TMap<FString, FVariant>>& dataTemp = data["ItemBase"];
	TMap<FString, FVariant>& itemdata = dataTemp[idx];
	itemName = itemdata["ItemName"].GetValue<FString>();
	maxOverlapNumber = itemdata["ItemOverlap"].GetValue<int32>();
	itemPrice = itemdata["ItemPrice"].GetValue<int32>();
	itemType = EItemType::Ammunition;
}