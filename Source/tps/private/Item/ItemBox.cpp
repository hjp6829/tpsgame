// Fill out your copyright notice in the Description page of Project Settings.


#include "Item/ItemBox.h"
#include "Item/HillItem.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Pawn/Player/MainPlayer.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/StaticMesh.h"
#include "Components/BoxComponent.h"
#include "Managers/GameItemManager.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Item/ItemBase.h"
// Sets default values
AItemBox::AItemBox()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	itemBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemBody"));
	itemBody->SetupAttachment(RootComponent);
	boxComp=CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
	boxComp->SetupAttachment(itemBody);
}

void AItemBox::Interact(AMainPlayer* player)
{
	UPlayerInventoryComoponent* inventory = player->GetCharacterComponent<UPlayerInventoryComoponent>();
	inventory->AddItem(saveItem);
	Destroy();
}

void AItemBox::InteractEnd()
{

}

void AItemBox::SetBody(FString path)
{
	UStaticMesh* itemMesh = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), nullptr, *path));
	if(itemMesh)
		itemBody->SetStaticMesh(itemMesh);
}

void AItemBox::SetItemData(class UItemBase* item)
{
	saveItem=item;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* serviceLocatorSystem = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	AGameItemManager* itemmanager = serviceLocatorSystem->GetService<AGameItemManager>();
	SetBody(item->GetItemPath());
}

void AItemBox::SetNewItemBoxPos(FVector newPos)
{
	SetActorLocation(newPos);
}

void AItemBox::EnableActor()
{
	itemBody->SetSimulatePhysics(false);
	itemBody->SetCollisionEnabled( ECollisionEnabled::NoCollision);
	boxComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AItemBox::ableActor()
{
	itemBody->SetSimulatePhysics(true);
	itemBody->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	boxComp->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
}

// Called when the game starts or when spawned
void AItemBox::BeginPlay()
{
	Super::BeginPlay();
}

