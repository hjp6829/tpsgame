// Fill out your copyright notice in the Description page of Project Settings.


#include "Item/ItemStore.h"
#include "UI/Player/ItemPurchasePopup.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Pawn/Player/MainPlayer.h"
#include "Weapon/WeaponBase.h"
#include "GameInstance/MainGameInstance.h"
#include "Managers/UIManagerServeSystem.h"
// Sets default values
AItemStore::AItemStore()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

void AItemStore::Interact(AMainPlayer* player)
{
	if(isWidgetOpen)
		return;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UUIManagerServeSystem* uimanager = gameInstance->GetSubsystem<UUIManagerServeSystem>();
	UItemPurchasePopup* storeWidget = uimanager->GetUI<UItemPurchasePopup>(EUIType::ItemStore);
	storeWidget->SetStoreData(player,this);
	storeWidget->OpenWidget();
	currentPlayer = player;
	isWidgetOpen = true;
	UMainGameInstance* maininstance = Cast<UMainGameInstance>(gameInstance);
	maininstance->ToggleMouseCursor(true);
}

void AItemStore::InteractEnd()
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UUIManagerServeSystem* uimanager = gameInstance->GetSubsystem<UUIManagerServeSystem>();
	UItemPurchasePopup* storeWidget = uimanager->GetUI<UItemPurchasePopup>(EUIType::ItemStore);
	if(storeWidget)
		storeWidget->CloseWidget();
}

void AItemStore::SellItem(EWeaponSlot weaponslot)
{
	UPlayerInventoryComoponent* inventoryComp = currentPlayer->GetCharacterComponent<UPlayerInventoryComoponent>();
	TMap<EWeaponSlot, class AWeaponBase*>& playerequips = inventoryComp->GetEquipWeaponMap();
	int value= playerequips[weaponslot]->GetWeaponPrice();
	playerequips[weaponslot]->ResetWeapon();
	playerequips[weaponslot] =nullptr;
}

void AItemStore::BuyWeaponItem(int32 money, EWeaponSlot weaponslot)
{
	UGameInstance* gameinstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* serviceLocator = gameinstance->GetSubsystem<UServiceLocatorServeSystem>();
	UPlayerInventoryComoponent* inventoryComp = currentPlayer->GetCharacterComponent<UPlayerInventoryComoponent>();
	//inventoryComp->SetWeaponEquip(weaponslot, weapon);
}

void AItemStore::BuyBulletItem(int32 money)
{
}

void AItemStore::SetIsWidget(bool value)
{
	isWidgetOpen = value;
}

// Called when the game starts or when spawned
void AItemStore::BeginPlay()
{
	Super::BeginPlay();
}

