#include "Item/ItemFactory.h"
#include "Item/ItemBase.h"
#include "Item/HillItem.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Managers/GameItemManager.h"
#include "Kismet/GameplayStatics.h"
#include "Item/AmmunitionItem.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/SubSystem/DataManagerSubsystem.h"
#include "Item/EquipItem.h"
UItemBase* ItemFactory::CreateItem(int32 idx, int32 num, UWorld* world)
{
	UItemBase* item = nullptr;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(world);
	UDataManagerSubsystem* dataManager = gameInstance->GetSubsystem<UDataManagerSubsystem>();
	if (dataManager->CheckContainDB("ItemBase"))
	{
		TMap<int, TMap<FString, FVariant>>& dataTemp = dataManager->GetTableData("ItemBase");
		TMap<FString, FVariant>& data = dataTemp[idx];
		EItemType EnumVariable = static_cast<EItemType>(data["ItemType"].GetValue<int32>());
		switch (EnumVariable)
		{
		case EItemType::Hill:
		{
			UHillItem* hillTemp = NewObject<UHillItem>();
			hillTemp->SetItemData(dataManager->GetData(), idx);
			hillTemp->SetNowItemNumber(num);
			item = hillTemp;
			break;
		}
		case EItemType::Ammunition:
		{
			UAmmunitionItem* AmmunitionTemp = NewObject<UAmmunitionItem>();
			AmmunitionTemp->SetItemData(dataManager->GetData(), idx);
			AmmunitionTemp->SetNowItemNumber(num);
			item = AmmunitionTemp;
			break;
		}
		case EItemType::WeaponEquip:
			UEquipItem* EquipTemp = NewObject<UEquipItem>();
			EquipTemp->SetItemData(dataManager->GetData(), idx);
			EquipTemp->SetNowItemNumber(num);
			item = EquipTemp;
			break;
		}
	}
	return item;
}
