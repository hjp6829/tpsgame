// Fill out your copyright notice in the Description page of Project Settings.


#include "Item/EquipItem.h"

float UEquipItem::GetMuzzleEquipData()
{
	return muzzleData;
}

int32 UEquipItem::GetMagazineEquipData()
{
	return magazineData;
}

float UEquipItem::GetStockEquipData()
{
	return stockData;
}

ERangeWeaponAttachmentType UEquipItem::GetEquipTypeData()
{
	return equipType;
}
void UEquipItem::SetItemData(TMap<FString, TMap<int, TMap<FString, FVariant>>>& data, int32 idx)
{
	Super::SetItemData(data, idx);
	TMap<int, TMap<FString, FVariant>>& dataTemp = data["ItemBase"];
	TMap<FString, FVariant>& itemdata = dataTemp[idx];
	itemName = itemdata["ItemName"].GetValue<FString>();
	maxOverlapNumber = itemdata["ItemOverlap"].GetValue<int32>();
	itemPrice = itemdata["ItemPrice"].GetValue<int32>();
	if (data.Contains("WeaponEquipItem"))
	{
		TMap<int, TMap<FString, FVariant>>& foreignDataTemp = data["WeaponEquipItem"];
		TMap<FString, FVariant>& foreignData = foreignDataTemp[idx];
		muzzleData= foreignData["Muzzle"].GetValue<float>();
		magazineData = foreignData["Magazine"].GetValue<int32>();
		stockData = foreignData["Stock"].GetValue<float>();
		equipType = static_cast<ERangeWeaponAttachmentType>(foreignData["EquipType"].GetValue<int32>());
	}
	itemType = EItemType::WeaponEquip;
}