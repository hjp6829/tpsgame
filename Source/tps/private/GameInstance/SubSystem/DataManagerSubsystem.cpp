// Fill out your copyright notice in the Description page of Project Settings.


#include "GameInstance/SubSystem/DataManagerSubsystem.h"
#include "SQLiteDatabase.h"
bool UDataManagerSubsystem::CheckContainDB(FString tableName)
{
	return datavalues.Contains(tableName);
}
TMap<int, TMap<FString, FVariant>>& UDataManagerSubsystem::GetTableData(FString tableName)
{
	return datavalues[*tableName];
}
TMap<FString, TMap<int, TMap<FString, FVariant>>>& UDataManagerSubsystem::GetData()
{
	return datavalues;
}
void UDataManagerSubsystem::LoadDataDB()
{
	const FString dbFilePath = FPaths::ProjectContentDir() + "Database/testDatabase.db";
	const FString tableMasterSQL = TEXT("SELECT name FROM sqlite_master WHERE type = 'table';");
	
	if (FSQLiteDatabase* mydb = new FSQLiteDatabase;
		mydb->Open(*dbFilePath, ESQLiteDatabaseOpenMode::ReadOnly))
	{
		TArray<FString> tableNames;
		if (FSQLitePreparedStatement* prePareStatement = new FSQLitePreparedStatement();
			prePareStatement->Create(*mydb, *tableMasterSQL, ESQLitePreparedStatementFlags::Persistent))
		{

			while (prePareStatement->Step() == ESQLitePreparedStatementStepResult::Row)
			{
				FString tableName;
				if (prePareStatement->GetColumnValueByIndex(0, tableName) && tableName != TEXT("sqlite_sequence"))
				{
					tableNames.Add(tableName);
				}
			}
			prePareStatement->Destroy();
		}
		for (FString name : tableNames)
		{
			FString tableSQL = FString::Printf(TEXT("SELECT * FROM %s"), *name);
			if (FSQLitePreparedStatement* prePareStatement = new FSQLitePreparedStatement();
				prePareStatement->Create(*mydb, *tableSQL, ESQLitePreparedStatementFlags::Persistent))
			{
				TMap<FString, FVariant> values;
				TMap<int, TMap<FString, FVariant>> datatemp;
				int32 idx;
				while (prePareStatement->Step() == ESQLitePreparedStatementStepResult::Row)
				{
					TArray<FString> filedNames = prePareStatement->GetColumnNames();

					for (FString filed : filedNames)
					{
						int32 intValue;
						float floatValue;
						FString stringValue;
						ESQLiteColumnType type;
						prePareStatement->GetColumnTypeByName(*filed, type);
						switch (type)
						{
						case ESQLiteColumnType::Integer:
							prePareStatement->GetColumnValueByName(*filed, intValue);
							if (filed == "IDX")
								idx = intValue;
							else
								values.Add(filed, intValue);
							break;
						case ESQLiteColumnType::Float:
							prePareStatement->GetColumnValueByName(*filed, floatValue);
							values.Add(filed, floatValue);
							break;
						case ESQLiteColumnType::String:
							prePareStatement->GetColumnValueByName(*filed, stringValue);
							values.Add(filed, stringValue);
							break;
						default:
							break;
						}
					}
					datatemp.Add(idx, values);
				}
				datavalues.Add(name, datatemp);
				prePareStatement->Destroy();
			}
		}
		mydb->Close();
		delete mydb;
	}
}

void UDataManagerSubsystem::Initialize(FSubsystemCollectionBase& Collection)
{
	Super::Initialize(Collection);
	LoadDataDB();
}
