// Fill out your copyright notice in the Description page of Project Settings.


#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Managers/GameItemManager.h"
void UServiceLocatorServeSystem::RemoveNullServices()
{
	TArray<UClass*> KeysToRemove;
	for (auto& Elem : Services)
	{
		if (!IsValid(Elem.Value))
			KeysToRemove.Add(Elem.Key);
	}
	for (UClass* Key : KeysToRemove)
	{
		Services.Remove(Key);
	}
}
UObject* UServiceLocatorServeSystem::GetItemManager()
{
	if (Services.Contains(AGameItemManager::StaticClass()))
	{
		return Services[AGameItemManager::StaticClass()];
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("%s Can't Find Service."), *AGameItemManager::StaticClass()->GetName());
		return nullptr;
	}
}