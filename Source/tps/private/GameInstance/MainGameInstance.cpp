// Fill out your copyright notice in the Description page of Project Settings.


#include "GameInstance/MainGameInstance.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"

//#define CUSTOM_LOG(Pos)UE_LOG(LogTemp,Warning,TEXT("VectorPos : %f , %f , %f"),Pos.X,Pos.Y,Pos.Z);

void UMainGameInstance::Init()
{
	Super::Init();
}

void UMainGameInstance::ToggleMouseCursor(bool value)
{	
	APlayerController* controller = GetWorld()->GetFirstPlayerController();
	if (controller)
	{
		controller->bShowMouseCursor=value;
	}
}

UServiceLocatorServeSystem* UMainGameInstance::GetlocatorSystem()
{
	UServiceLocatorServeSystem* serLocator = GetSubsystem<UServiceLocatorServeSystem>();
	return serLocator;
}


