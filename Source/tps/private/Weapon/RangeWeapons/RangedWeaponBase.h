// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/WeaponBase.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Interfaces/Interfaces.h"
#include "RangedWeaponBase.generated.h"
USTRUCT(Atomic, BlueprintType)
struct FAttachmentSlotData
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	bool isOpenSlot=true;
	IEquipable* equipItem=nullptr;
};
USTRUCT(Atomic, BlueprintType)
struct FRangeWeaponAttachmentSlot
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere)
	TMap<ERangeWeaponAttachmentType, FAttachmentSlotData> slotDatas;
	IEquipable* SetItem(class IEquipable* item, ERangeWeaponAttachmentType type);
};
/**
 * 
 */
UCLASS()
class ARangedWeaponBase : public AWeaponBase
{
	GENERATED_BODY()
public: 
virtual void BeginPlay()override;
virtual void InitializeItem()override;
virtual bool CheckCanReload()override;
int32 GetUsedBulletConut();
int32 GetMaxMagazine();
int32 GetAmmunition();
void SetInventoryBullet(int32 value);
void ResetUsedBullrtCount();
bool CheckDefaultBulletLeft();
void GetItemData();
FRangeWeaponAttachmentSlot& GetWeaponAttachment();
int32 GetCurrentBulletIDX();
IEquipable* SetEquipItem(class IEquipable* item,ERangeWeaponAttachmentType type);
protected:
	float attackCoolTime=0;
	int32 maxMagazine=0;
	int32 ammunitionNumber=0;
	int32 damage=0;
	float currentSpreadRadius = 0;
	float maxSpreadRadius = 3;
	float spreadIncrement = 0.5f;
	int32 currentBulletIDX=0;
	FTimerHandle AttackCoolTimer;
	class USkeletalMeshComponent* gunSkeletalMesh;
	int32 UsedBulletCount=0;
	int32 invenctoryBulletConut=0;
	int32 defaultBulletCount=0;
	UPROPERTY(EditAnywhere)
	FRangeWeaponAttachmentSlot attachmentSlotData;
		UPROPERTY(EditAnywhere)
	TSubclassOf<class AActor> effectClass;
	float recoilValue=0;
protected:
	virtual void FireBullet() PURE_VIRTUAL(ARangedWeaponBase::FireBullet,);
	virtual void ReloadWeapon() override;
};
