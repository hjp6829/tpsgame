// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/RangeWeapons/ShotgunWeapon.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Pawn/CustomCharacter.h"
#include "Kismet/GameplayStatics.h"
AShotgunWeapon::AShotgunWeapon()
{

}

void AShotgunWeapon::FireBullet()
{
	if (!gunSkeletalMesh)
		return;
	if (ammunitionNumber == 0)
		return;
	if (!gunSkeletalMesh->DoesSocketExist(TEXT("FirePos")))
		return;
	FTransform firePos = gunSkeletalMesh->GetSocketTransform(TEXT("FirePos"));
	FVector fireStartPos = firePos.GetLocation();
	ACustomCharacter* ownerCharacter = Cast<ACustomCharacter>(Owner);
	FVector fireEndPos = ownerCharacter->GetFireEndCheckVector(10000);
	float spreadX = 0;
	float spreadY = 0;


	FHitResult hitResult;
	FCollisionQueryParams params;
	ECollisionChannel channel = ECC_GameTraceChannel1;
	bool hit = GetWorld()->LineTraceSingleByChannel(hitResult, ownerCharacter->GetFireEndCheckStartPos(), fireEndPos, channel, params);

	FVector shootDirection = hit ? hitResult.ImpactPoint - fireStartPos : fireEndPos - fireStartPos;
	shootDirection.Normalize();
	FVector upVector = FVector::UpVector;
	FVector rightVector = FVector::CrossProduct(upVector, shootDirection); //외적을 사용해서 발사방향의 오른쪽을 구함
	rightVector.Normalize();

	for (int32 i = 0; i < buckshotCount; i++)
	{
		if (currentSpreadRadius > 0.0f)
		{
			float randomAngle = FMath::RandRange(0.0f, 360.0f);
			float randomRadius = FMath::RandRange(0.0f, currentSpreadRadius);
			spreadX = randomRadius * FMath::Cos(randomAngle);
			spreadY = randomRadius * FMath::Sin(randomAngle);
		}
		//업,오른쪽 백터를 사용해서 발사체에 수직인 평면을 만든다음 거기에 spread를 곱해주어 랜덤으로 총알을 발사
		FVector finalSpread = rightVector * spreadX + upVector * spreadY;
		FVector finalShootDirection = (shootDirection + finalSpread).GetSafeNormal();
		bool hitTarget = GetWorld()->LineTraceSingleByChannel(hitResult, fireStartPos, fireStartPos + finalShootDirection * 10000, channel, params);
		if (hitTarget)
		{
			GetWorld()->SpawnActor<AActor>(effectClass, hitResult.ImpactPoint, FRotator::ZeroRotator);
			TSubclassOf<UDamageType> damageTypeClass = UDamageType::StaticClass();

			UGameplayStatics::ApplyDamage(
				hitResult.GetActor(),
				damage,
				ownerCharacter->GetController(),
				ownerCharacter,
				damageTypeClass
			);
		}
	}
	isAttack = true;
	ammunitionNumber--;
	if (defaultBulletCount == 0)
		UsedBulletCount++;
	else
		defaultBulletCount--;
	OnAttack.Broadcast(currentBulletIDX, 1);
	GetWorld()->GetTimerManager().SetTimer(AttackCoolTimer, this, &AShotgunWeapon::SetCanAttack, attackCoolTime, false);
	GetWorld()->GetTimerManager().ClearTimer(SpreadRadiusDecreaseHandle);
	if (currentSpreadRadius <= maxSpreadRadius)
		currentSpreadRadius += spreadIncrement;
	ownerCharacter->AddCameraRotation(FRotator(recoilValue, 0, 0));
}

void AShotgunWeapon::AttackStart(float yaw, float pitch)
{
	if (!isAttack)
		FireBullet();
}

void AShotgunWeapon::AttackRelease()
{
	
}
void AShotgunWeapon::BeginPlay()
{
	Super::BeginPlay();
	gunSkeletalMesh = FindComponentByClass<USkeletalMeshComponent>();
	buckshotCount=5;
	currentSpreadRadius=0.05f;
}
void AShotgunWeapon::SetCanAttack()
{
	GetWorld()->GetTimerManager().ClearTimer(StartSpreadRadiusDecreaseHandle);
	isAttack = false;
	GetWorld()->GetTimerManager().SetTimer(StartSpreadRadiusDecreaseHandle, this, &AShotgunWeapon::StartSpreadRadiusDecrease, attackCoolTime +1, false);
}
void AShotgunWeapon::StartSpreadRadiusDecrease()
{
	GetWorld()->GetTimerManager().SetTimer(SpreadRadiusDecreaseHandle, this, &AShotgunWeapon::SpreadRadiusDecrease, 0.1f, true);
}

void AShotgunWeapon::SpreadRadiusDecrease()
{
	if (currentSpreadRadius <= 0.05f)
		GetWorld()->GetTimerManager().ClearTimer(SpreadRadiusDecreaseHandle);
	if (currentSpreadRadius >= 0)
		currentSpreadRadius -= spreadIncrement;

}
