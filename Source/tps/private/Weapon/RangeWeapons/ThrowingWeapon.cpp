// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/RangeWeapons/ThrowingWeapon.h"
#include "Weapon/WeaponBullets/GrenadeBullet.h"
// Sets default values
AThrowingWeapon::AThrowingWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void AThrowingWeapon::AttackStart(float yaw, float pitch)
{
	FVector lineStartPos = GetActorLocation();
	FVector LineEndPos;
	for (int i = 0; i < 20; i++)
	{
		float distanceX = speed * FMath::Cos(FMath::DegreesToRadians(pitch)) * FMath::Cos(FMath::DegreesToRadians(yaw)) * i;
		float distanceY = speed * FMath::Cos(FMath::DegreesToRadians(pitch)) * FMath::Sin(FMath::DegreesToRadians(yaw)) * i;
		float distanceZ = speed * FMath::Sin(FMath::DegreesToRadians(pitch)) * i - 0.5f * 9.81f * (i * i);
		FVector newPos = FVector(distanceX, distanceY, distanceZ);
		LineEndPos = newPos + GetActorLocation();
		DrawDebugLine(GetWorld(), lineStartPos, LineEndPos, FColor::Red, false, 0.1f, 0, 1);
		lineStartPos = LineEndPos;
	}
	launchDirection =FRotator(pitch,yaw,0);
}

void AThrowingWeapon::AttackRelease()
{
	//DetachRootComponentFromParent(true);
	AGrenadeBullet* bullet = GetWorld()->SpawnActor<AGrenadeBullet>(bulletClsss, GetActorLocation(), FRotator::ZeroRotator);
	bullet->FireGrenade(launchDirection,speed);
	OnUpdateUsedBulletInventory.Broadcast(5,1);
}

// Called when the game starts or when spawned
void AThrowingWeapon::BeginPlay()
{
	Super::BeginPlay();
}

