// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/RangeWeapons/RifleWeapon.h"
#include "Components/StaticMeshComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/SkeletalMeshComponent.h"
#include "Pawn/CustomCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimMontage.h"
ARifleWeapon::ARifleWeapon()
{
	//USceneComponent* DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	//RootComponent = DefaultRootComponent;
	//ConstructorHelpers::FObjectFinder<UStaticMesh> MeshAsset(TEXT("Engine.StaticMesh'/Game/Model/Mesh/RifleMesh.RifleMesh'"));
	//if (MeshAsset.Succeeded())
	//{
	//	UStaticMeshComponent* ChildComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ChildComponent"));
	//	ChildComponent->SetStaticMesh(MeshAsset.Object);
	//	ChildComponent->SetupAttachment(RootComponent);
	//	ChildComponent->SetRelativeScale3D(FVector(0.3f));
	//}
}

void ARifleWeapon::FireBullet()
{
	if(!gunSkeletalMesh)
		return;
	if(ammunitionNumber==0)
		return;
	if (!gunSkeletalMesh->DoesSocketExist(TEXT("FirePos")))
		return;
	FTransform firePos = gunSkeletalMesh->GetSocketTransform(TEXT("FirePos"));
	FVector fireStartPos= firePos.GetLocation();
	ACustomCharacter* ownerCharacter = Cast<ACustomCharacter>(Owner);
	FVector fireEndPos = ownerCharacter->GetFireEndCheckVector(1000);
	float spreadX=0;
	float spreadY=0;
	if(currentSpreadRadius>0.0f)
	{
		float randomAngle = FMath::RandRange(0.0f, 360.0f);
		float randomRadius = FMath::RandRange(0.0f, currentSpreadRadius);
		spreadX = randomRadius * FMath::Cos(randomAngle);
		spreadY = randomRadius * FMath::Sin(randomAngle);
	}

	FHitResult hitResult;
	FCollisionQueryParams params;
	ECollisionChannel channel=ECC_GameTraceChannel1;
	bool hit=GetWorld()->LineTraceSingleByChannel(hitResult, ownerCharacter->GetFireEndCheckStartPos(), fireEndPos,channel,params);

	FVector shootDirection = hit ? hitResult.ImpactPoint - fireStartPos : fireEndPos - fireStartPos;
	shootDirection.Normalize();
	FVector upVector = FVector::UpVector; 
	FVector rightVector = FVector::CrossProduct(upVector, shootDirection); //외적을 사용해서 발사방향의 오른쪽을 구함
	rightVector.Normalize();
	//업,오른쪽 백터를 사용해서 발사체에 수직인 평면을 만든다음 거기에 spread를 곱해주어 랜덤으로 총알을 발사
	FVector finalSpread = rightVector * spreadX + upVector * spreadY;
	FVector finalShootDirection = (shootDirection + finalSpread).GetSafeNormal();

	bool hitTarget = GetWorld()->LineTraceSingleByChannel(hitResult, fireStartPos, fireStartPos+ finalShootDirection*10000, channel, params);

	if (hitTarget)
	{
		GetWorld()->SpawnActor<AActor>(effectClass, hitResult.ImpactPoint, FRotator::ZeroRotator);
		TSubclassOf<UDamageType> damageTypeClass=UDamageType::StaticClass();

		UGameplayStatics::ApplyDamage(
		hitResult.GetActor(),
		damage,
		ownerCharacter->GetController(),
		ownerCharacter,
		damageTypeClass
		);
	}
	isAttack=true;
	ammunitionNumber--;
	if(defaultBulletCount==0)
		UsedBulletCount++;
	else
		defaultBulletCount--;
	OnAttack.Broadcast(currentBulletIDX, 1);
	GetWorld()->GetTimerManager().SetTimer(AttackCoolTimer, this, &ARifleWeapon::SetCanAttack, attackCoolTime, false);
	GetWorld()->GetTimerManager().ClearTimer(SpreadRadiusDecreaseHandle);
	if(currentSpreadRadius<= maxSpreadRadius)
		currentSpreadRadius+= spreadIncrement;
	ownerCharacter->AddCameraRotation(FRotator(recoilValue,0,0));
	UAnimInstance* animInstance = ownerCharacter->GetMesh()->GetAnimInstance();
	if (animInstance && attackMontage)
	{
		animInstance->Montage_Play(attackMontage);
	}
}

void ARifleWeapon::AttackStart(float yaw, float pitch)
{
	if(!isAttack)
		FireBullet();
}

void ARifleWeapon::AttackRelease()
{
	Super::AttackRelease();
	GetWorld()->GetTimerManager().ClearTimer(StartSpreadRadiusDecreaseHandle);
	GetWorld()->GetTimerManager().ClearTimer(SpreadRadiusDecreaseHandle);
	currentSpreadRadius=0;
}

void ARifleWeapon::BeginPlay()
{
	Super::BeginPlay();
	gunSkeletalMesh = FindComponentByClass<USkeletalMeshComponent>();

}

void ARifleWeapon::SetCanAttack()
{
	GetWorld()->GetTimerManager().ClearTimer(StartSpreadRadiusDecreaseHandle);
	isAttack=false;
	GetWorld()->GetTimerManager().SetTimer(StartSpreadRadiusDecreaseHandle, this, &ARifleWeapon::StartSpreadRadiusDecrease, 1, false);
}

void ARifleWeapon::StartSpreadRadiusDecrease()
{
	GetWorld()->GetTimerManager().SetTimer(SpreadRadiusDecreaseHandle, this, &ARifleWeapon::SpreadRadiusDecrease, 0.1f, true);
}

void ARifleWeapon::SpreadRadiusDecrease()
{
	if(currentSpreadRadius<=0)
		GetWorld()->GetTimerManager().ClearTimer(SpreadRadiusDecreaseHandle);
	if(currentSpreadRadius>=0)
		currentSpreadRadius-= spreadIncrement;

}
