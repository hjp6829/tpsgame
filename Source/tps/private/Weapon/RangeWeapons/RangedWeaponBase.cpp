// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "Managers/GameItemManager.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/SubSystem/DataManagerSubsystem.h"
void ARangedWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	GetItemData();
	maxSpreadRadius = maxSpreadRadius * 0.01f;
	spreadIncrement = spreadIncrement * 0.01f;
	recoilValue= recoilValue*0.1f;
}

void ARangedWeaponBase::InitializeItem()
{
	
}

bool ARangedWeaponBase::CheckCanReload()
{
	if(maxMagazine> ammunitionNumber)
		return true;
	return false;
}

int32 ARangedWeaponBase::GetUsedBulletConut()
{
	return UsedBulletCount;
}

int32 ARangedWeaponBase::GetMaxMagazine()
{
	return maxMagazine;
}
int32 ARangedWeaponBase::GetAmmunition()
{
	return ammunitionNumber;
}
void ARangedWeaponBase::SetInventoryBullet(int32 value)
{
	invenctoryBulletConut= value;
}

void ARangedWeaponBase::ResetUsedBullrtCount()
{	
	UsedBulletCount=0;
}

bool ARangedWeaponBase::CheckDefaultBulletLeft()
{
	if(defaultBulletCount==0)
		return false;
	return true;
}

void ARangedWeaponBase::GetItemData()
{
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UDataManagerSubsystem* dataManager = gameInstance->GetSubsystem<UDataManagerSubsystem>();
	if (dataManager->CheckContainDB("ItemBase"))
	{
		TMap<int, TMap<FString, FVariant>>& dataTemp = dataManager->GetTableData("ItemBase");
		TMap<FString, FVariant>& data = dataTemp[weaponIDX];
		itemName = data["ItemName"].GetValue<FString>();
		price = data["ItemPrice"].GetValue<int32>();
		itemType = static_cast<EItemType>(data["ItemType"].GetValue<int32>());
		FString foreignKey = data["ForeignKey"].GetValue<FString>();
		TMap<int, TMap<FString, FVariant>>& foreignTemp = dataManager->GetData()[foreignKey];
		TMap<FString, FVariant>& foreigndata = foreignTemp[weaponIDX];
		damage = foreigndata["Damage"].GetValue<int32>();
		reloadTime = foreigndata["ReloadTime"].GetValue<float>();
		maxSpreadRadius = foreigndata["MaxSpreadRadius"].GetValue<float>();
		spreadIncrement = foreigndata["SpreadIncrement"].GetValue<float>();
		attackCoolTime = foreigndata["AttackCoolTime"].GetValue<float>();
		maxMagazine = foreigndata["MaxMagazine"].GetValue<int32>();
		weaponType= static_cast<EWeaponType>(foreigndata["WeaponType"].GetValue<int32>());
		currentBulletIDX= foreigndata["BulletIDX"].GetValue<int32>();
		recoilValue= foreigndata["RecoilValue"].GetValue<int32>();
	}
	ammunitionNumber = maxMagazine;
}

FRangeWeaponAttachmentSlot& ARangedWeaponBase::GetWeaponAttachment()
{
	return attachmentSlotData;
}

int32 ARangedWeaponBase::GetCurrentBulletIDX()
{
	return currentBulletIDX;
}

IEquipable* ARangedWeaponBase::SetEquipItem(IEquipable* item, ERangeWeaponAttachmentType type)
{
	IEquipable* temp = attachmentSlotData.SetItem(item, type);
	switch (type)
	{
	case ERangeWeaponAttachmentType::MuzzleAttachment:
		if (temp != nullptr)
		{
			int32 muzzleData = temp->GetMuzzleEquipData();
			maxSpreadRadius -= muzzleData;
		}
		maxSpreadRadius -= item->GetMuzzleEquipData()*0.01f;

		break;
	case ERangeWeaponAttachmentType::MagazineAttachment:
		if (temp != nullptr)
		{
			int32 magazineData = temp->GetMagazineEquipData();
			maxMagazine -= magazineData;
		}
		maxMagazine+= item->GetMagazineEquipData();
		break;
	case ERangeWeaponAttachmentType::StockAttachment:
		if (temp != nullptr)
		{
			int32 stockData = temp->GetStockEquipData();
			recoilValue += stockData;
		}
		recoilValue -= item->GetStockEquipData()*0.1f;
		break;
	default:
		break;
	}
	return temp;
}

void ARangedWeaponBase::ReloadWeapon()
{
	Super::ReloadWeapon();
	if ((invenctoryBulletConut+ ammunitionNumber) - maxMagazine < 0)
	{
		ammunitionNumber= (invenctoryBulletConut + ammunitionNumber);
		OnAmmoReloaded.Broadcast(currentBulletIDX, invenctoryBulletConut);
		invenctoryBulletConut=0;
	}
	else
	{
		OnAmmoReloaded.Broadcast(currentBulletIDX, maxMagazine- ammunitionNumber);
		invenctoryBulletConut = invenctoryBulletConut - (maxMagazine - ammunitionNumber);
		ammunitionNumber= maxMagazine;
	}

}

IEquipable* FRangeWeaponAttachmentSlot::SetItem(IEquipable* item, ERangeWeaponAttachmentType type)
{
	IEquipable* tempItem= slotDatas[type].equipItem;
	slotDatas[type].equipItem=item;
	return tempItem;
}
