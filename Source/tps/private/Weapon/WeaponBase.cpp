// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/WeaponBase.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/GameItemManager.h"
#include "Managers/GameItemManager.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
// Sets default values
AWeaponBase::AWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	resetPos=GetActorLocation();
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EWeaponType AWeaponBase::GetWeaponType()
{
	return weaponType;
}

void AWeaponBase::ResetWeapon()
{
	OnAttack.Clear();
	OnAmmoReloaded.Clear();

	DetachRootComponentFromParent(true);
	SetActorLocation(resetPos);
	SetOwner(nullptr);
	UGameInstance* gameinstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* serviceLocator = gameinstance->GetSubsystem<UServiceLocatorServeSystem>();
	AGameItemManager* itemManager = serviceLocator->GetService<AGameItemManager>();
	itemManager->InWeaponToPool(this,weaponType);
}

int32 AWeaponBase::GetWeaponPrice()
{
	return price;
}

float AWeaponBase::GetReloadTime()
{
	return reloadTime;
}

FString AWeaponBase::GetWeaponName()
{
	return itemName;
}

UAnimMontage* AWeaponBase::GetMeleeMontage()
{
	return meleeMontage;
}

