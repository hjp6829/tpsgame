// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/SpawnItemBase.h"
#include "Interfaces/Enums.h"
#include "WeaponBase.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FWeapnAttack, int idx, int value);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnAmmoReloadedDelegate, int idx, int value);
DECLARE_MULTICAST_DELEGATE_TwoParams(FUsedBulletNum, int idx, int value);
UCLASS()
class AWeaponBase : public ASpawnItemBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponBase();
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	EWeaponType GetWeaponType();
	virtual void ResetWeapon();
	virtual void AttackStart(float yaw, float pitch) {};
	virtual void InitializeWeapon() {};
	virtual void AttackRelease() {};
	virtual void ReloadWeapon() {};
	virtual bool CheckCanReload()
	{
		return true;
	};
	int32 GetWeaponPrice();
	float GetReloadTime();
	virtual void InitializeItem(){};
	FString GetWeaponName();
	class UAnimMontage* GetMeleeMontage();
public:
	FWeapnAttack OnAttack;
	FOnAmmoReloadedDelegate OnAmmoReloaded;
	FUsedBulletNum OnUpdateUsedBulletInventory;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
protected:
	UPROPERTY(EditAnywhere)
	int32 weaponIDX;
	EWeaponType weaponType;
	EItemType itemType;
	UPROPERTY()
	int32 price=0;
	UPROPERTY()
	float reloadTime=3.0f;
	UPROPERTY()
	FString itemName;
	UPROPERTY(EditAnywhere)
	class UAnimMontage* attackMontage;
	UPROPERTY(EditAnywhere)
	class UAnimMontage* meleeMontage;
private:
	FVector resetPos;
	
};
