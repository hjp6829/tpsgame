// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/WeaponBullets/GrenadeBullet.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
// Sets default values
AGrenadeBullet::AGrenadeBullet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	collisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComp"));
	collisionComp->SetCollisionProfileName(TEXT("BlockAll"));
	collisionComp->SetSphereRadius(5);
	RootComponent = collisionComp;
	movementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComp"));
	movementComp->SetUpdatedComponent(collisionComp);
}

void AGrenadeBullet::FireGrenade(FRotator dir,float speed)
{
	FVector launchVelocity = dir.Vector() * speed * 10;
	movementComp->ProjectileGravityScale = 1;
	movementComp->InitialSpeed = 5000;
	movementComp->MaxSpeed = 5000;
	movementComp->Activate(true);
	movementComp->Velocity = launchVelocity;
	OnActorHit.AddDynamic(this,&AGrenadeBullet::HitTest);
}

// Called when the game starts or when spawned
void AGrenadeBullet::BeginPlay()
{
	Super::BeginPlay();
	
}

void AGrenadeBullet::HitTest(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	UE_LOG(LogTemp,Warning,TEXT("Hit Granada"));
}

