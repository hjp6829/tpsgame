// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/WeaponBullets/Bullet.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
// Sets default values
ABullet::ABullet()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	collisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionComp"));
	collisionComp->SetCollisionProfileName(TEXT("BlockAll"));
	collisionComp->SetSphereRadius(13);
	RootComponent = collisionComp;
	bodymeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BodymeshComp"));
	bodymeshComp->SetupAttachment(RootComponent);
	bodymeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	bodymeshComp->SetRelativeScale3D(FVector(0.25f));
	movementComp = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComp"));
	movementComp->SetUpdatedComponent(collisionComp);
}

void ABullet::SetBulletSpeed(float speed)
{
	movementComp->InitialSpeed=speed;
	movementComp->MaxSpeed=speed;
	movementComp->Activate(true);
}

// Called when the game starts or when spawned
void ABullet::BeginPlay()
{
	Super::BeginPlay();
}

