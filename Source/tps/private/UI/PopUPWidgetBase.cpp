// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/PopUPWidgetBase.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/MainGameInstance.h"
#include "Managers/GameModeBases/MainGameModeBase.h"
void UPopUPWidgetBase::OpenPopUpWidget()
{
	if (isVisibleMouse)
	{
		UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
		UMainGameInstance* maininstance = Cast<UMainGameInstance>(gameInstance);
		maininstance->ToggleMouseCursor(true);
	}
	if (isLockMouse)
	{
		AMainGameModeBase* mainGameBase = Cast<AMainGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		mainGameBase->ToggleMouseInput(false);
	}
	SetVisibility(ESlateVisibility::Visible);
}

void UPopUPWidgetBase::ClosePopUpWidget()
{
	if (isVisibleMouse)
	{
		UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
		UMainGameInstance* maininstance = Cast<UMainGameInstance>(gameInstance);
		maininstance->ToggleMouseCursor(false);
	}
	if (isLockMouse)
	{
		AMainGameModeBase* mainGameBase = Cast<AMainGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		mainGameBase->ToggleMouseInput(true);
	}
	SetVisibility(ESlateVisibility::Hidden);
}
