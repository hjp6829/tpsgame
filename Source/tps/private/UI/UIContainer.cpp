// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/UIContainer.h"
#include "UI/BaseWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/UIManagerServeSystem.h"
// Sets default values
AUIContainer::AUIContainer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

}

UBaseWidget* AUIContainer::GetUI(EUIType type)
{
	UBaseWidget** tempUI = UIs.FindByPredicate([&](UBaseWidget* ui)
		{
			return ui && ui->GetUIType() == type;
		});
	return *tempUI;
}

void AUIContainer::ToggleUI_Container(EUIType type)
{
	UBaseWidget** tempUI = UIs.FindByPredicate([&](UBaseWidget* ui)
		{
			return ui && ui->GetUIType() == type;
		});
	if (tempUI)
	{
		if ((*tempUI)->GetVisibility() == ESlateVisibility::Visible)
		{
			(*tempUI)->CloseWidget();
		}
		else if((*tempUI)->GetVisibility() == ESlateVisibility::Hidden)
		{
			(*tempUI)->OpenWidget();
		}
	}
}

// Called when the game starts or when spawned
void AUIContainer::BeginPlay()
{
	Super::BeginPlay();
	for (int32 i = 0; i < UIsClass.Num(); i++)
	{
		UIs.Add(CreateWidget<UBaseWidget>(GetWorld(), UIsClass[i]));
	}
	for (int32 i = 0; i < UIs.Num(); i++)
	{
		if (UIs[i]->GetStartView())
			UIs[i]->OpenWidget();
	}
	UGameInstance* instanceTemp = UGameplayStatics::GetGameInstance(GetWorld());
	UUIManagerServeSystem* uimanager = instanceTemp->GetSubsystem<UUIManagerServeSystem>();
	uimanager->SetUIContainer(this);
}

