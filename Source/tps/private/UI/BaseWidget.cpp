// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/BaseWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/GameModeBases/MainGameModeBase.h"
bool UBaseWidget::OpenWidget()
{
	if (isPlayerMouseInput)
	{
		AMainGameModeBase* mainGameBase = Cast<AMainGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		mainGameBase->ToggleMouseInput(false);
	}
	isOpen=true;
	AddToViewport();
	return true;
}

bool UBaseWidget::CloseWidget()
{
	if(!isOpen)
		return false;
	if (isPlayerMouseInput)
	{
		AMainGameModeBase* mainGameBase = Cast<AMainGameModeBase>(UGameplayStatics::GetGameMode(GetWorld()));
		mainGameBase->ToggleMouseInput(true);
	}
	isOpen = false;
	RemoveFromViewport();
	return true;
}

EUIType UBaseWidget::GetUIType()
{
	return uitype;
}

bool UBaseWidget::GetStartView()
{
	return isStartAdd;
}

bool UBaseWidget::CheckOpenWidget()
{
	return isOpen;
}
