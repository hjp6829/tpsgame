// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/ItemStoreWidget.h"
#include "Components/Button.h"
#include "Interfaces/Enums.h"
#include "UI/WeaponEquipWidget.h"
#include "Item/ItemStore.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/MainGameInstance.h"
#include "Components/CanvasPanel.h"
#include "Pawn/Player/MainPlayer.h"
#include "UI/Player/ItemPurchasePopup.h"
void UItemStoreWidget::SetStoreData(AMainPlayer* player, AItemStore* stroe)
{
	//WeaponEquiPopUp->SetWidgetData(playerinventoryComp->GetEquipWeaponMap(), stroe, this);
	//ItemPurchasePopUp->SetPlayer(player);
	Itemstroe=stroe;
}
void UItemStoreWidget::SetWeaponWidget(UWeaponEquipWidget* Widget)
{
	WeaponEquiPopUp = Widget;
}
void UItemStoreWidget::NativeConstruct()
{
	Super::NativeConstruct();
	UE_LOG(LogTemp, Warning, TEXT("asdasd"));
	if(!bIsEventBinded)
	{
		if (BulletButton)
			BulletButton->OnClicked.AddDynamic(this, &UItemStoreWidget::OnClickBulletList);
		bIsEventBinded=true;
	}
	WeaponEquiPopUp=Cast<UWeaponEquipWidget>(WeaponEquipWidget);
	ItemPurchasePopUp = Cast<UItemPurchasePopup>(ItemPurchaseWidget);
}

void UItemStoreWidget::OnClickWeaponList()
{
	WeaponEquiPopUp->OpenPopUpWidget();
}

void UItemStoreWidget::OnClickBulletList()
{
	//if(ItemPurchasePopUp)
	//	{ItemPurchasePopUp->OpenPopUpWidget();}
	//	else
	//	UE_LOG(LogTemp,Warning,TEXT("Null"));
}

bool UItemStoreWidget::CloseWidget()
{
	if(!Super::CloseWidget())
	return false;
	WeaponEquiPopUp->SetVisibility(ESlateVisibility::Hidden);
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UMainGameInstance* maininstance = Cast<UMainGameInstance>(gameInstance);
	maininstance->ToggleMouseCursor(false);
	Itemstroe->SetIsWidget(false);
	return true;
}

void UItemStoreWidget::ToggleBackGround(bool value)
{
	if(value)
		BackGround->SetVisibility(ESlateVisibility::Visible);
	else
		BackGround->SetVisibility(ESlateVisibility::Hidden);
}

