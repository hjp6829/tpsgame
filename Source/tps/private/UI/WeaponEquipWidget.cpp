// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/WeaponEquipWidget.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Item/ItemStore.h"
#include "UI/ItemStoreWidget.h"

void UWeaponEquipWidget::SetWidgetData(TMap<EWeaponSlot, class AWeaponBase*>& playerequips, AItemStore* store, class UItemStoreWidget* stroewidget)
{
	itemStore= store;
	equips= playerequips;
	itemStoreWidget= stroewidget;
	UGameInstance* gameinstance = UGameplayStatics::GetGameInstance(GetWorld());

}

void UWeaponEquipWidget::testWeaponBuy()
{
	
}

void UWeaponEquipWidget::SellWeapon()
{
	itemStore->SellItem(currentWeaponSlot);
}

void UWeaponEquipWidget::SetNowEquipWeapon(EWeaponSlot weaponslot)
{
	currentWeaponSlot = weaponslot;
}

void UWeaponEquipWidget::SetWeaponSelect(int32 idx)
{
	selectWeaponIDX=idx;
}

void UWeaponEquipWidget::ClosePopUPWidget()
{
	SetVisibility(ESlateVisibility::Hidden);
	itemStoreWidget->ToggleBackGround(true);
}
