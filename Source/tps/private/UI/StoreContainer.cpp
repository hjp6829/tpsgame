// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/StoreContainer.h"

UStoreContainer::~UStoreContainer()
{
UE_LOG(LogTemp,Warning,TEXT("UStoreContainer Destroy"));
}

TMap<FString, FVariant>& UStoreContainer::GetData()
{
	return *data;
}

void UStoreContainer::SetData(TMap<FString, FVariant>& newdata)
{
	data= &newdata;
}

void UStoreContainer::SetITemIDX(int32 idx)
{
	itemIDX=idx;
}

int32 UStoreContainer::GetItemIDX()
{
	return itemIDX;
}
