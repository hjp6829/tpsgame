// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Button/ItemListButton.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Interfaces/Enums.h"
#include "Item/HillItem.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/GameItemManager.h"
#include "UI/StoreContainer.h"
void UItemListButton::NativeOnListItemObjectSet(UObject* ListItemObject)
{
	itemContainer = Cast<UStoreContainer>(ListItemObject);
	TMap<FString, FVariant>& itemData = itemContainer->GetData();
	ItemName->SetText(FText::FromString(itemData["ItemName"].GetValue<FString>()));
	if (itemButton)
	{
		itemButton->OnClicked.AddDynamic(this, &UItemListButton::Buttonclick);
	}
}

void UItemListButton::Buttonclick()
{
	itemContainer->OnButtonClick.Execute(itemContainer);
}

