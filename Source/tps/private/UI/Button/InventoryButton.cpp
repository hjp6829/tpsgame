// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Button/InventoryButton.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Pawn/CustomCharacter.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Components/Image.h"
#include "Managers/ImageManager.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Kismet/GameplayStatics.h"
void UInventoryButton::ChangeButtonColor(FLinearColor color)
{

}

void UInventoryButton::SetData(FInventorySlot& slot,ACustomCharacter* character)
{
	ownerCharacter= character;
	slotData= &slot;
	if(slotData!=nullptr)
	{
		buttonText->SetText(FText::FromString(FString(FString::FromInt(slot.item->GetNowItemNumber()))));
		UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
		UServiceLocatorServeSystem* serviceLocatorSystem = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
		AImageManager* imageManager = serviceLocatorSystem->GetService<AImageManager>();
		UTexture2D* texture = imageManager->GetItemImage(slotData->GetItemIDX());
		ItemImage->SetBrushFromTexture(texture);
	}
	else
	{
		buttonText->SetText(FText::FromString(FString("0")));
		ItemImage->SetBrushFromTexture(nullptr);
	}

	
}

FInventorySlot UInventoryButton::GetInventorySlot()
{
	if(slotData)
		return *slotData;
	return FInventorySlot();
}
int32 UInventoryButton::GetInventoryItemIDX()
{
	if(slotData)
		return slotData->GetItemIDX();
	return -1;
}
ACustomCharacter* UInventoryButton::GetCharacter()
{
	if(ownerCharacter)
		return ownerCharacter;
	return nullptr;
}
void UInventoryButton::SetItemImage(UImage* image)
{
	ItemImage=image;
}
void UInventoryButton::UseItem()
{
	if (slotData== nullptr)
		return;
	IItemAction* itemAction = Cast<IItemAction>(slotData->item);
	itemAction->ItemAction(ownerCharacter);
	UPlayerInventoryComoponent* inventoryComp = ownerCharacter->GetCharacterComponent<UPlayerInventoryComoponent>();
	inventoryComp->UseItem(slotData->GetItemIDX(),1);
}
