// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Player/DropArea.h"
#include "Managers/GameItemManager.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Item/ItemBox.h"
#include "Interfaces/Enums.h"
#include "Pawn/CustomCharacter.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Item/ItemBase.h"
bool UDropArea::SetDropItemData(UItemBase* itembase,ACustomCharacter* character,int32 idx)
{
	if (itembase == nullptr)
		return false;
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* serviceLocatorSystem = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	AGameItemManager* itemmanager = serviceLocatorSystem->GetService<AGameItemManager>();
	AItemBox* itembox = itemmanager->GetItemFromPool<AItemBox>(EPoolType::SpawnitemBase);
	FVector location = character->GetActorLocation();
	location = location + character->GetActorForwardVector() * 100;
	itembox->SetItemData(itembase);
	itembox->SetNewItemBoxPos(location);
	UPlayerInventoryComoponent* inventory = character->GetCharacterComponent<UPlayerInventoryComoponent>();
	inventory->SetNULLIDX(idx);
	return true;
}
