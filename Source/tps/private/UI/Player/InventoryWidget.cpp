// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Player/InventoryWidget.h"
#include "UI/Button/InventoryButton.h"
#include "Components/UniformGridPanel.h"
#include "Components/UniformGridSlot.h"
#include "Styling/SlateBrush.h"
#include "UI/Player/EquippedWeaponWidget.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
void UInventoryWidget::SetInventoryList(UPlayerInventoryComoponent* inventory,ACustomCharacter* character)
{
	playerInventoryComp= inventory;
	playerInventoryList = &inventory->GetInventory();
	ownerCharacter= character;
}
void UInventoryWidget::OpenPopUpWidget()
{
	Super::OpenPopUpWidget();
	UpdateInventorySlot();
	UpdateWeaponSlot();
}
void UInventoryWidget::NativeConstruct()
{
	for (int32 i = 0; i < 2; i++)
	{
		for (int32 j = 0; j < 10; j++)
		{
			UInventoryButton* buttonTemp=CreateWidget<UInventoryButton>(GetWorld(), inventoryButton);
			inventorySlot.Add(buttonTemp);
			UUniformGridSlot* GridSlot=InventoryButtonGrid->AddChildToUniformGrid(buttonTemp, i, j);
			GridSlot->SetHorizontalAlignment(HAlign_Fill);
			GridSlot->SetVerticalAlignment(VAlign_Fill);
		}
	}
	mainWeaponWidget=Cast<UEquippedWeaponWidget>(MainWeapon);
	secondaryWeaponWidget = Cast<UEquippedWeaponWidget>(SecondaryWeapon);
}

void UInventoryWidget::UpdateInventorySlot()
{
	for (int32 i = 0; i < playerInventoryList->Num(); i++)
	{
		FInventorySlot* slot= (*playerInventoryList)[i];
		inventorySlot[i]->SetData(*slot, ownerCharacter);
	}
}

void UInventoryWidget::UpdateWeaponSlot()
{
	TMap<EWeaponSlot, class AWeaponBase*>& weaponSlots = playerInventoryComp->GetWeaponSlot();
	ARangedWeaponBase* mainWeapon = Cast<ARangedWeaponBase>(weaponSlots[EWeaponSlot::Main]);
	if (mainWeapon)
	{
		mainWeaponWidget->SetWeaponData(*mainWeapon);
	}
	ARangedWeaponBase* secondaryeapon = Cast<ARangedWeaponBase>(weaponSlots[EWeaponSlot::Secondary]);
	if (secondaryeapon)
	{
		secondaryWeaponWidget->SetWeaponData(*secondaryeapon);
	}
}
