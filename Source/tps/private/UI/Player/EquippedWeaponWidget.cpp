// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Player/EquippedWeaponWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "UI/Player/InventoryWeaponEquipSlot.h"
void UEquippedWeaponWidget::SetWeaponData(ARangedWeaponBase& data)
{
	TMap<ERangeWeaponAttachmentType, FAttachmentSlotData>& slotDatas= data.GetWeaponAttachment().slotDatas;
	WeaponName->SetText(FText::FromString(data.GetWeaponName()));
	UInventoryWeaponEquipSlot* MuzzleAttachWidget = Cast<UInventoryWeaponEquipSlot>(MuzzleAttachment);
	UInventoryWeaponEquipSlot* MagazineAttachClass = Cast<UInventoryWeaponEquipSlot>(MagazineAttachment);
	UInventoryWeaponEquipSlot* StockAttachClass = Cast<UInventoryWeaponEquipSlot>(StockAttachment);
	MuzzleAttachWidget->SetWeaponData(data);
	MagazineAttachClass->SetWeaponData(data);
	StockAttachClass->SetWeaponData(data);
	if (!slotDatas[ERangeWeaponAttachmentType::MuzzleAttachment].isOpenSlot)
	{
		MuzzleAttachWidget->SetVisibility(ESlateVisibility::Hidden);
	}
	if (!slotDatas[ERangeWeaponAttachmentType::MagazineAttachment].isOpenSlot)
	{
		MagazineAttachClass->SetVisibility(ESlateVisibility::Hidden);
	}
	if (!slotDatas[ERangeWeaponAttachmentType::StockAttachment].isOpenSlot)
	{
		StockAttachClass->SetVisibility(ESlateVisibility::Hidden);
	}
}
