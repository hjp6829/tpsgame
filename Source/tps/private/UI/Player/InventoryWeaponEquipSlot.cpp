// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Player/InventoryWeaponEquipSlot.h"
#include "Item/ItemBase.h"
#include "Pawn/CustomCharacter.h"
#include "Item/EquipItem.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Managers/ImageManager.h"
#include "Components/Image.h"
bool UInventoryWeaponEquipSlot::SetData(UItemBase* item, ACustomCharacter* character, int32 idx)
{
	IEquipable* equip = Cast<IEquipable>(item);
	if (!equip)
		return false;
	if(equip->GetEquipTypeData()!= equipType)
		return false;
	IEquipable* tempequip = currentWeapon->SetEquipItem(equip, equip->GetEquipTypeData());
	UPlayerInventoryComoponent* inventoryComp = character->GetCharacterComponent<UPlayerInventoryComoponent>();
	inventoryComp->SetNULLIDX(idx);
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* serviceLocatorSystem = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	AImageManager* imageManager = serviceLocatorSystem->GetService<AImageManager>();
	UTexture2D* texture = imageManager->GetItemImage(item->GetIDX());
	itemImage->SetBrushFromTexture(texture);
	if(tempequip!=nullptr)
		inventoryComp->AddItem(Cast<UItemBase>(tempequip));
	return true;
}

void UInventoryWeaponEquipSlot::SetWeaponData(ARangedWeaponBase& weapon)
{
	currentWeapon= &weapon;
}

void UInventoryWeaponEquipSlot::SetItemImage(UImage* image)
{
	itemImage=image;
}
