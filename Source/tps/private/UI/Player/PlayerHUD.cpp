// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Player/PlayerHUD.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/CanvasPanel.h"
#include "UI/Player/InventoryWidget.h"
#include "Pawn/Player/PlayerComponent/PlayerStateComponent.h"
#include "Managers/GameModeBases/MainGameModeBase.h"
#include "Pawn/Player/MainPlayer.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
void UPlayerHUD::CrosshairVisibility(bool value)
{
	if(value)
	{
	Crosshair->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
	Crosshair->SetVisibility(ESlateVisibility::Hidden);
	}
}

void UPlayerHUD::SetEquipWeaponBullet(int32 value, int32 weaponvalue)
{
	CachedInventoryBulletCount =value;
	CachedWeaponBulletCount= weaponvalue;
	EquipBullet->SetText(FText::FromString(FString(FString::FromInt(weaponvalue)+"/"+ FString::FromInt(value))));
}

void UPlayerHUD::UpdateCachedBullet(int32 value)
{
	CachedWeaponBulletCount -=value;
	//CachedInventoryBulletCount-= value;
	EquipBullet->SetText(FText::FromString(FString(FString::FromInt(CachedWeaponBulletCount) + "/" + FString::FromInt(CachedInventoryBulletCount))));
}

void UPlayerHUD::UpdateReloadBullet(int32 value)
{
	CachedWeaponBulletCount += value;
	CachedInventoryBulletCount -= value;
	EquipBullet->SetText(FText::FromString(FString(FString::FromInt(CachedWeaponBulletCount) + "/" + FString::FromInt(CachedInventoryBulletCount))));
}

void UPlayerHUD::UpdateThrowWeaponNum(int32 num)
{
	ThrowWeaponNumText->SetText(FText::FromString(FString(FString::FromInt(num))));
}

void UPlayerHUD::TogglePlayerInventory()
{
	if(HUDBG->GetVisibility()== ESlateVisibility::Visible)
	{
		HUDBG->SetVisibility(ESlateVisibility::Hidden);
		UPlayerInventoryComoponent* inventory = player->GetCharacterComponent<UPlayerInventoryComoponent>();
		inventoryPopUpWidget->SetInventoryList(inventory,player);
		inventoryPopUpWidget->OpenPopUpWidget();
		isInventoryOpen=true;
	}
	else if (HUDBG->GetVisibility() == ESlateVisibility::Hidden)
	{
		HUDBG->SetVisibility(ESlateVisibility::Visible);
		inventoryPopUpWidget->ClosePopUpWidget();
		isInventoryOpen=false;
	}
}

void UPlayerHUD::SetOwnerPlayer(AMainPlayer* ownerPlayer)
{
	player= ownerPlayer;
	UPlayerStateComponent* stateComp = player->GetCharacterComponent<UPlayerStateComponent>();
	UpdateCharacterHP(stateComp->GetMaxHP(), stateComp->GetHP());
	stateComp->OnHPUpdate.AddUObject(this,&UPlayerHUD::UpdateCharacterHP);
	UPlayerInventoryComoponent* inventoryComp = player->GetCharacterComponent<UPlayerInventoryComoponent>();
	inventoryComp->OnInventoryUpdate.AddUObject(this,&UPlayerHUD::UpdateInventoryUI);
}

void UPlayerHUD::UpdateCharacterHP(int32 maxhp,int32 hp)
{ 
	FString hptext=FString::FromInt(hp)+" / "+ FString::FromInt(maxhp);
	CharacterHPTextBox->SetText(FText::FromString(hptext));
}

void UPlayerHUD::UpdateInventoryUI()
{
	if (isInventoryOpen)
		inventoryPopUpWidget->UpdateInventorySlot();
}

void UPlayerHUD::NativeConstruct()
{
	Super::NativeConstruct();
	inventoryPopUpWidget=Cast<UInventoryWidget>(InventoryBG);
	
}
