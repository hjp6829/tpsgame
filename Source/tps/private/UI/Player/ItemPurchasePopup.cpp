// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Player/ItemPurchasePopup.h"
#include "Kismet/GameplayStatics.h"
#include "GameInstance/SubSystem/DataManagerSubsystem.h"
#include "Components/ListView.h"
#include "UI/StoreContainer.h"
#include "Interfaces/Enums.h"
#include "Components/TextBlock.h"
#include "Pawn/Player/MainPlayer.h"
#include "Pawn/Player/PlayerComponent/PlayerStateComponent.h"
#include "Pawn/Player/PlayerComponent/PlayerInventoryComoponent.h"
#include "Item/ItemFactory.h"
#include "GameInstance/MainGameInstance.h"
#include "Item/ItemStore.h"
#include "Managers/ImageManager.h"
#include "GameInstance/SubSystem/ServiceLocatorServeSystem.h"
#include "Components/Image.h"
bool UItemPurchasePopup::OpenWidget()
{
	if(!Super::OpenWidget())
		return false;
	UGameInstance* gameinstance = UGameplayStatics::GetGameInstance(GetWorld());
	UDataManagerSubsystem* dataManager = gameinstance->GetSubsystem<UDataManagerSubsystem>();
	TMap<int, TMap<FString, FVariant>>& temp=dataManager->GetTableData("ItemBase");
	for (auto& elem : temp)
	{
		TMap<FString, FVariant>& innermap= elem.Value;
		if(static_cast<EItemType>(innermap["ItemType"].GetValue<int32>())== EItemType::Weapon )
			continue;
		FString itenName= innermap["ItemName"].GetValue<FString>();
		UStoreContainer* container= NewObject<UStoreContainer>();
		container->OnButtonClick.BindUObject(this,&UItemPurchasePopup::SetAddItem);
		container->SetData(innermap);
		container->SetITemIDX(elem.Key);
		ItemList->AddItem(container);
		itemContainers.Add(container);
	}
	UPlayerStateComponent* stateComp=currentPlayer->GetCharacterComponent<UPlayerStateComponent>();
	FString goldData="0 / "+ FString::FromInt(stateComp->GetCharacterGold());
	GoldTextBox->SetText(FText::FromString(goldData));
	ItemImage->SetBrushFromTexture(nullptr);
	return true;
}

bool UItemPurchasePopup::CloseWidget()
{
	if (!Super::CloseWidget())
		return false;
		UE_LOG(LogTemp,Warning,TEXT("asd"));
	ItemList->ClearListItems();
	for (int32 i = 0; i < itemContainers.Num(); i++)
	{
		itemContainers[i]->ConditionalBeginDestroy();
	}
	itemContainers.Empty();
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UMainGameInstance* maininstance = Cast<UMainGameInstance>(gameInstance);
	maininstance->ToggleMouseCursor(false);
	Itemstroe->SetIsWidget(false);
	return true;
}

void UItemPurchasePopup::SetStoreData(AMainPlayer* player, AItemStore* stroe)
{
	currentPlayer = player;
	Itemstroe= stroe;
}

void UItemPurchasePopup::SetAddItem(UStoreContainer* itemContainer)
{
	currentItemContainer= itemContainer;
	TMap<FString, FVariant>& itemData = itemContainer->GetData();
	itemIDX= itemContainer->GetItemIDX();
	FString temp = itemData["ItemName"].GetValue<FString>();
	itemPrice = itemData["ItemPrice"].GetValue<int32>();
	FString priceText = FString::FromInt(itemPrice);
	FString itemdatastring = "ItemName : " + temp + "\n";
	ItemData->SetText(FText::FromString(itemdatastring));
	UPlayerStateComponent* stateComp = currentPlayer->GetCharacterComponent<UPlayerStateComponent>();
	playerGold= stateComp->GetCharacterGold();
	FString goldData = priceText + " / " + FString::FromInt(playerGold);
	GoldTextBox->SetText(FText::FromString(goldData));
	UGameInstance* gameInstance = UGameplayStatics::GetGameInstance(GetWorld());
	UServiceLocatorServeSystem* serviceLocatorSystem = gameInstance->GetSubsystem<UServiceLocatorServeSystem>();
	AImageManager* imageManager = serviceLocatorSystem->GetService<AImageManager>();
	UTexture2D* texture = imageManager->GetItemImage(itemIDX);
	ItemImage->SetBrushFromTexture(texture);
}

void UItemPurchasePopup::OnBuyItem()
{
	if(itemPrice>= playerGold)
		return;

	UItemBase* itemTemp = ItemFactory::CreateItem(itemIDX,1,GetWorld());
	UPlayerInventoryComoponent* inventoryComp = currentPlayer->GetCharacterComponent<UPlayerInventoryComoponent>();
	UPlayerStateComponent* stateComp = currentPlayer->GetCharacterComponent<UPlayerStateComponent>();
	inventoryComp->AddItem(itemTemp);
	stateComp->UseGold(itemPrice);
	playerGold= stateComp->GetCharacterGold();
	TMap<FString, FVariant>& itemData = currentItemContainer->GetData();
	itemPrice = itemData["ItemPrice"].GetValue<int32>();
	FString priceText = FString::FromInt(itemPrice);
	FString goldData = priceText + " / " + FString::FromInt(playerGold);
	GoldTextBox->SetText(FText::FromString(goldData));
}
