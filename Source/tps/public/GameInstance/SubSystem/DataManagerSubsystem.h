// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "DataManagerSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UDataManagerSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
public:
	bool CheckContainDB(FString tableName);
	TMap<int, TMap<FString, FVariant>>& GetTableData(FString tableName);
	TMap<FString, TMap<int, TMap<FString, FVariant>>>& GetData();
private:
	void LoadDataDB();
	TMap<FString, TMap<int, TMap<FString, FVariant>>>datavalues;
	virtual void Initialize(FSubsystemCollectionBase& Collection)override;
};
