// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MainGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UMainGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
virtual void Init()override;
void ToggleMouseCursor(bool value);
UFUNCTION(BlueprintCallable)
class UServiceLocatorServeSystem* GetlocatorSystem();
};
