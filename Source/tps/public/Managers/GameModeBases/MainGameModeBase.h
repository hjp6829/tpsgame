// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainGameModeBase.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FMouseInput, bool value);
DECLARE_MULTICAST_DELEGATE_OneParam(FMoveInput, bool value);
/**
 * 
 */
UCLASS()
class TPS_API AMainGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	void ToggleMouseInput(bool value);
	void ToggleMoveInput(bool value);
public:
	FMouseInput OnToggleMouseInput;
	FMoveInput OnToggleMoveInput;
protected:
 virtual void BeginPlay() override;
private:
};
