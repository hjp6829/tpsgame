// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Managers/ManagerBase.h"
#include "ImageManager.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API AImageManager : public AManagerBase
{
	GENERATED_BODY()
public:
	class UTexture2D* GetItemImage(int32 idx);
protected:
	virtual void BeginPlay() override;
private:
UPROPERTY(EditAnywhere)
TMap<int32,class UTexture2D*> itemImageMap;
};
