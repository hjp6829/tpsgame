// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Enums.h"
#include "Item/SpawnItemBase.h"
#include "ItemPool.generated.h"

UCLASS()
class TPS_API AItemPool : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemPool();
	ASpawnItemBase* GetPoolItem();
	void InPoolItem(ASpawnItemBase* item);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:
UPROPERTY(EditAnywhere,Category="Data")
 TSubclassOf<ASpawnItemBase> itemClass;
 UPROPERTY(EditAnywhere,Category="Data")
int32 spawnNum=10;
TQueue<ASpawnItemBase*> actorQueue;
};




