// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Enums.h"
#include "WeaponFactory.generated.h"

UCLASS()
class TPS_API AWeaponFactory : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponFactory();
	class AWeaponBase* GetWeapon(EAttackType attackType, EWeaponType weaponType);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:
	UPROPERTY(EditAnywhere,Category="WeaponClass")
	TArray<TSubclassOf<class ARangedWeaponBase>> RangeWeaponClass;
	UPROPERTY(EditAnywhere,Category="WeaponClass")
	TArray<TSubclassOf<class AMeleeWaeponBase>> MeleeWeaponClass;

	UPROPERTY()
	TMap<EWeaponType, class ARangedWeaponBase*>RangeWeapons;
	UPROPERTY()
	TMap<EWeaponType, class AMeleeWaeponBase*>MeleeWeapons;
};
