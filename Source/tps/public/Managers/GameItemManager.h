// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Managers/ManagerBase.h"
#include "Item/ItemBase.h"
#include "Managers/ItemPool.h"
#include "GameItemManager.generated.h"

UCLASS()
class TPS_API AGameItemManager : public AManagerBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameItemManager();
	template<typename T>
	T* GetItemFromPool(EPoolType type);
	template<typename T>
	T* GetWeaponFromPool(EWeaponType type);
	void InItemToPool(class ASpawnItemBase* item, EPoolType type);
	void InWeaponToPool(class ASpawnItemBase* item, EWeaponType type);
protected:
	virtual void BeginPlay() override;
private:
	UPROPERTY(EditAnywhere)
	TMap<EPoolType,class AItemPool*> itemPoolList;
	UPROPERTY(EditAnywhere)
	TMap<EWeaponType,class AItemPool*> WeaponPoolList;
	TMap<int32,class UItemDataBaseContainer*> itemData;
};

template<typename T>
inline T* AGameItemManager::GetItemFromPool(EPoolType type)
{
	if(itemPoolList.Contains(type))
	{
		return Cast<T>(itemPoolList[type]->GetPoolItem());
	}
	else
	{
		UE_LOG(LogTemp,Warning,TEXT("PoolIsNull"));
		return nullptr;
	}
}

template<typename T>
inline T* AGameItemManager::GetWeaponFromPool(EWeaponType type)
{
	if (WeaponPoolList.Contains(type))
	{
		return Cast<T>(WeaponPoolList[type]->GetPoolItem());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("PoolIsNull"));
		return nullptr;
	}
}


