// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/ItemBase.h"
#include "AmmunitionItem.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UAmmunitionItem : public UItemBase
{
	GENERATED_BODY()
public:
	virtual void SetItemData(TMap<FString, TMap<int, TMap<FString, FVariant>>>& data, int32 idx)override;
};
