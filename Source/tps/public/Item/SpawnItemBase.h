// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnItemBase.generated.h"

UCLASS()
class TPS_API ASpawnItemBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnItemBase();
	virtual void InitializeItem(){};
	virtual void SetBody(FString path){};
	virtual void EnableActor(){};
	virtual void ableActor() {};
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
protected:
	
};
