// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Interfaces.h"
#include "Interfaces/Enums.h"
#include "Item/ItemBase.h"
#include "ItemStore.generated.h"

UCLASS()
class TPS_API AItemStore : public AActor,public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemStore();
	virtual void Interact(class AMainPlayer* player)override;
	virtual void InteractEnd()override;
	void SellItem(EWeaponSlot WeaponSlot);
	void BuyWeaponItem(int32 money, EWeaponSlot WeaponSlot);
	void BuyBulletItem(int32 money);
	void SetIsWidget(bool value);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private: 
 bool isWidgetOpen=false;
 class AMainPlayer* currentPlayer;
};
