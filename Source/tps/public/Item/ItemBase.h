// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Interfaces/Enums.h"
#include "Interfaces/Interfaces.h"
#include "ItemBase.generated.h"
/**
 * 
 */
UCLASS()
class TPS_API UItemBase : public UObject,public IItemAction
{
	GENERATED_BODY()
public:
	int32 getMaxOverlapNumber();
	int32 GetIDX();
	EItemType GetItemType();
	FString GetItemPath();
	void SetNowItemNumber(int32 num);
	int32 GetNowItemNumber();
public:
	virtual void SetItemData(TMap<FString, TMap<int, TMap<FString, FVariant>>>& data,int32 idx);
	virtual void ItemAction(class ACustomCharacter* character)
	{
		UE_LOG(LogTemp,Warning,TEXT("ItemBaseUse"));
	}

protected:
	FString itemName;
	EItemType itemType;
	int32 maxOverlapNumber;
	int32 nowverlapNumber;
	int32 itemIDX;
	int32 itemPrice;
	FString itemPath;
};
