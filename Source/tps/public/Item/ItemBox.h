// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/SpawnItemBase.h"
#include "Interfaces/Interfaces.h"
#include "ItemBox.generated.h"

UCLASS()
class TPS_API AItemBox : public ASpawnItemBase, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemBox();
	virtual void Interact(class AMainPlayer* player)override;
	virtual void InteractEnd()override;
	virtual void SetBody(FString path)override;
	void SetItemData(class UItemBase* item);
	void SetNewItemBoxPos(FVector newPos);
	virtual void EnableActor()override;
	virtual void ableActor()override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
		UPROPERTY(EditAnywhere)
		TSubclassOf<class UItemBase> itemClass;
		UPROPERTY(EditAnywhere)
	class UStaticMeshComponent* itemBody;
	UPROPERTY(EditAnywhere)
	class UBoxComponent* boxComp;
	class UItemBase* saveItem;
};
