// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interfaces/Enums.h"
/**
 * 
 */
class TPS_API ItemFactory
{
public:
	static class UItemBase* CreateItem(int32 idx,int32 num,UWorld* world);
};
