// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/ItemBase.h"
#include "Interfaces/Interfaces.h"
#include "HillItem.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UHillItem : public UItemBase
{
	GENERATED_BODY()
public:
	virtual void ItemAction(class ACustomCharacter* character)override;
	virtual void SetItemData(TMap<FString, TMap<int, TMap<FString, FVariant>>>& data, int32 idx)override;
protected:
 int32 hillAmount=0;
};
