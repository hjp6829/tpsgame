// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item/ItemBase.h"
#include "Interfaces/Interfaces.h"
#include "EquipItem.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UEquipItem : public UItemBase,public IEquipable
{
	GENERATED_BODY()
public:
	virtual float GetMuzzleEquipData()override;
	virtual int32 GetMagazineEquipData()override;
	virtual float GetStockEquipData()override;
	virtual ERangeWeaponAttachmentType GetEquipTypeData()override;
	virtual void SetItemData(TMap<FString, TMap<int, TMap<FString, FVariant>>>& data,int32 idx)override;
private:
	float muzzleData=0;
	int32 magazineData = 0;
	float stockData = 0;
	ERangeWeaponAttachmentType equipType;
};
