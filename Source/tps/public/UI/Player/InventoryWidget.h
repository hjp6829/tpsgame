// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/PopUPWidgetBase.h"
#include "Pawn/Components/Base/InventoryComponent.h"
#include "InventoryWidget.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UInventoryWidget : public UPopUPWidgetBase
{
	GENERATED_BODY()
public:
	void SetInventoryList(class UPlayerInventoryComoponent* inventory, class ACustomCharacter* character);
	virtual void OpenPopUpWidget()override;
	void UpdateInventorySlot();
protected:
	virtual void NativeConstruct()override;
private:
 TArray<class UInventoryButton*> inventorySlot;
 	UPROPERTY(meta=(BindWidget))
	class UUniformGridPanel* InventoryButtonGrid;
	UPROPERTY(meta=(BindWidget))
	class UUserWidget* MainWeapon;
	UPROPERTY(meta=(BindWidget))
	class UUserWidget* SecondaryWeapon;
	class UEquippedWeaponWidget* mainWeaponWidget;
	class UEquippedWeaponWidget* secondaryWeaponWidget;
	UPROPERTY(EditAnywhere)
	TSubclassOf<class UInventoryButton> inventoryButton;
	TArray<FInventorySlot*>* playerInventoryList;
	class ACustomCharacter* ownerCharacter;
	class UPlayerInventoryComoponent* playerInventoryComp;
private:

	void UpdateWeaponSlot();
};
