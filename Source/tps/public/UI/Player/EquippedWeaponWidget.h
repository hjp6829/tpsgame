// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "EquippedWeaponWidget.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UEquippedWeaponWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	void SetWeaponData(ARangedWeaponBase& data);
private:
	UPROPERTY(meta=(BindWidget))
	class UTextBlock* WeaponName;
	UPROPERTY(meta=(BindWidget))
	class UUserWidget* MuzzleAttachment;
		UPROPERTY(meta=(BindWidget))
	class UUserWidget* MagazineAttachment;
		UPROPERTY(meta=(BindWidget))
	class UUserWidget* StockAttachment;
};
