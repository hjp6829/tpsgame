// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseWidget.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UPlayerHUD : public UBaseWidget
{
	GENERATED_BODY()
public:
 UPROPERTY(meta=(BindWidget))
 class UImage* Crosshair;
 UPROPERTY()
 int32 CachedInventoryBulletCount=0;
  UPROPERTY()
 int32 CachedWeaponBulletCount=0;
public:
 void CrosshairVisibility(bool value);
 void SetEquipWeaponBullet(int32 value, int32 weaponvalue);
 void UpdateCachedBullet(int32 value);
 void UpdateReloadBullet(int32 value);
 void UpdateThrowWeaponNum(int32 num);
 void TogglePlayerInventory();
 void SetOwnerPlayer(class AMainPlayer* ownerPlayer);
 void UpdateCharacterHP(int32 maxhp, int32 hp);
 UFUNCTION()
 void UpdateInventoryUI();
protected:
	virtual void NativeConstruct()override;
private:
 UPROPERTY(meta=(BindWidget))
 class UTextBlock* EquipBullet;
  UPROPERTY(meta=(BindWidget))
 class UTextBlock* ThrowWeaponNumText;
  UPROPERTY(meta=(BindWidget))
 class UCanvasPanel* HUDBG;
  UPROPERTY(meta=(BindWidget))
 class UPopUPWidgetBase* InventoryBG;
 UPROPERTY(meta=(BindWidget))
 class UTextBlock* CharacterHPTextBox;
 class UInventoryWidget* inventoryPopUpWidget;
 class AMainPlayer* player;
 bool isInventoryOpen=false;
};
