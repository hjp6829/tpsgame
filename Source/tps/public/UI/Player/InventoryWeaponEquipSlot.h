// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Interfaces/Enums.h"
#include "InventoryWeaponEquipSlot.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UInventoryWeaponEquipSlot : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	bool SetData(class UItemBase* item,class ACustomCharacter* character,int32 idx);
	void SetWeaponData(class ARangedWeaponBase& weapon);
	UFUNCTION(BlueprintCallable)
	void SetItemImage(class UImage* image);
private:
 class ARangedWeaponBase* currentWeapon;
 UPROPERTY(EditAnywhere)
 ERangeWeaponAttachmentType equipType;
 class UImage* itemImage;
};
