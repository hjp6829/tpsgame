// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Item/ItemBase.h"
#include "DropArea.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UDropArea : public UUserWidget
{
	GENERATED_BODY()
	public:
	UFUNCTION(BlueprintCallable)
	bool SetDropItemData(class UItemBase* itembase, ACustomCharacter* character, int32 idx);
};
