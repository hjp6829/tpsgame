// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseWidget.h"
#include "ItemPurchasePopup.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UItemPurchasePopup : public UBaseWidget
{
	GENERATED_BODY()
	public:
		virtual bool OpenWidget()override;
		virtual bool CloseWidget()override;
		void SetStoreData(class AMainPlayer* player, class AItemStore* stroe);
private:
 void SetAddItem(class UStoreContainer* itemContainer);
 UFUNCTION(BlueprintCallable)
 void OnBuyItem();
private:
 UPROPERTY(meta=(BindWidget))
 class UListView* ItemList;
  UPROPERTY(meta=(BindWidget))
 class UImage* ItemImage;
   UPROPERTY(meta=(BindWidget))
 class UTextBlock* ItemData;
  UPROPERTY(meta=(BindWidget))
 class UTextBlock* GoldTextBox;
   UPROPERTY(meta=(BindWidget))
 class UButton* BuyButton;
 class AMainPlayer* currentPlayer;
 int32 itemPrice;
 int32 playerGold;
 int32 itemIDX;
 class UStoreContainer* currentItemContainer;
 TArray<UStoreContainer*> itemContainers;
 class AItemStore* Itemstroe;
};
