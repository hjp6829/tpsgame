// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PopUPWidgetBase.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UPopUPWidgetBase : public UUserWidget
{
	GENERATED_BODY()
public:
UFUNCTION(BlueprintCallable)
	virtual void OpenPopUpWidget();
	UFUNCTION(BlueprintCallable)
	virtual void ClosePopUpWidget();
protected:
 UPROPERTY(EditAnywhere,Category="Setting")
 bool isVisibleMouse=false;
  UPROPERTY(EditAnywhere,Category="Setting")
 bool isLockMouse=false;
};
