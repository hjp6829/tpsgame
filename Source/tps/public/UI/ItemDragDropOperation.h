// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/DragDropOperation.h"
#include "ItemDragDropOperation.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UItemDragDropOperation : public UDragDropOperation
{
	GENERATED_BODY()
	public:
	UFUNCTION(BlueprintCallable)
	void SetDropData(class UItemBase* item,int32 IDX,class ACustomCharacter* owner);
	 UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Your Category")
	class UItemBase* itemBase;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Your Category")
	int32 inventoryIDX;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Your Category")
	class ACustomCharacter* character;
};
