// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "StoreContainer.generated.h"

/**
 * 
 */
DECLARE_DELEGATE_OneParam(FButtonClick, class UStoreContainer* container);
UCLASS()
class TPS_API UStoreContainer : public UObject
{
	GENERATED_BODY()
	public:
	~UStoreContainer();
		TMap<FString, FVariant>& GetData();
		void SetData(TMap<FString, FVariant>& newdata);
		void SetITemIDX(int32 idx);
		int32 GetItemIDX();
public:
	FButtonClick OnButtonClick;
	private:
		TMap<FString, FVariant>* data;
		int32 itemIDX;
};
