// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Pawn/Components/Base/InventoryComponent.h"
#include "InventoryButton.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UInventoryButton : public UUserWidget
{
	GENERATED_BODY()
public:
 void ChangeButtonColor(FLinearColor color);
 void SetData(FInventorySlot& slot, class ACustomCharacter* character);
 UFUNCTION(BlueprintCallable)
 FInventorySlot GetInventorySlot();
  UFUNCTION(BlueprintCallable)
 int32 GetInventoryItemIDX();
  UFUNCTION(BlueprintCallable)
  class ACustomCharacter* GetCharacter();
  UFUNCTION(BlueprintCallable)
  void SetItemImage(class UImage* image);
private: 
UPROPERTY(meta=(BindWidget))
class UTextBlock* buttonText;
class UImage* ItemImage;
FInventorySlot* slotData;
class ACustomCharacter* ownerCharacter;
private:
UFUNCTION(BlueprintCallable)
void UseItem();
};
