// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/IUserObjectListEntry.h"
#include "Item/ItemBase.h"
#include "ItemListButton.generated.h"

/**
 * 
 */

UCLASS()
class TPS_API UItemListButton : public UUserWidget, public IUserObjectListEntry
{
	GENERATED_BODY()
public:
	virtual void NativeOnListItemObjectSet(UObject* ListItemObject)override;

private: 
UPROPERTY(meta=(BindWidget))
class UTextBlock* ItemName;
UPROPERTY(meta = (BindWidget))
class UButton* itemButton;
class UStoreContainer* itemContainer;
private: 
UFUNCTION()
 void Buttonclick();
};
