// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/BaseWidget.h"
#include "Interfaces/Enums.h"
#include "ItemStoreWidget.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UItemStoreWidget : public UBaseWidget
{
	GENERATED_BODY()

public:
void SetStoreData(class AMainPlayer* player,class AItemStore* stroe);
UFUNCTION(BlueprintCallable)
void SetWeaponWidget(UWeaponEquipWidget* Widget);
virtual bool CloseWidget() override;
UFUNCTION(BlueprintCallable)
void ToggleBackGround(bool value);

protected:
virtual void NativeConstruct()override;

private:
 //UPROPERTY(meta=(BindWidget))
 //class UButton* WeaponButton;
  UPROPERTY(meta=(BindWidget))
 class UButton* BulletButton;
   UPROPERTY(meta=(BindWidget))
 class UCanvasPanel* BackGround;
 UPROPERTY(meta=(BindWidget))
 class UUserWidget* WeaponEquipWidget;
  UPROPERTY(meta=(BindWidget))
 class UUserWidget* ItemPurchaseWidget;
 class UItemPurchasePopup* ItemPurchasePopUp;
 class UWeaponEquipWidget* WeaponEquiPopUp;
 bool bIsEventBinded=false;
 class AItemStore* Itemstroe;

private:
UFUNCTION(BlueprintCallable)
void OnClickWeaponList();
UFUNCTION(BlueprintCallable)
void OnClickBulletList();

};
