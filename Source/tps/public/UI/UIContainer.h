// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interfaces/Enums.h"
#include "UIContainer.generated.h"

UCLASS()
class TPS_API AUIContainer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AUIContainer();
	class UBaseWidget* GetUI(EUIType type);
	void ToggleUI_Container(EUIType type);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:
UPROPERTY(EditAnywhere,Category="Uis")
	TArray<TSubclassOf<class UBaseWidget>> UIsClass;
	UPROPERTY(EditAnywhere,Category="Uis")
	TArray<class UBaseWidget*> UIs;
};
