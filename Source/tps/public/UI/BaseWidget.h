// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Interfaces/Enums.h"
#include "BaseWidget.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UBaseWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
	virtual bool OpenWidget();
	UFUNCTION(BlueprintCallable)
	virtual bool CloseWidget();
	UFUNCTION()
	EUIType GetUIType();
	UFUNCTION()
	bool GetStartView();
	bool CheckOpenWidget();
private:
 UPROPERTY(EditAnywhere)
 EUIType uitype;
  UPROPERTY(EditAnywhere)
 bool isStartAdd=false;
   UPROPERTY(EditAnywhere)
 bool isPlayerMouseInput=false;
   UPROPERTY(EditAnywhere)
   bool isPlayerMoveInput;
   bool isOpen=false;
};
