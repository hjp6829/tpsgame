// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UI/PopUPWidgetBase.h"
#include "Interfaces/Enums.h"
#include "Item/ItemBase.h"
#include "WeaponEquipWidget.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UWeaponEquipWidget : public UPopUPWidgetBase
{
	GENERATED_BODY()
public: 
void SetWidgetData(TMap<EWeaponSlot, class AWeaponBase*>& playerequips, class AItemStore* store,class UItemStoreWidget* stroewidget);
private:
	UPROPERTY(meta=(BindWidget))
	class UButton* Weapon1;
	UPROPERTY(meta=(BindWidget))
	class UButton* Weapon2;
	UPROPERTY(meta=(BindWidget))
	class UButton* Weapon3;
	UPROPERTY(meta=(BindWidget))
	class UButton* Weapon4;
	UPROPERTY(meta=(BindWidget))
	class UButton* Buy;
	UPROPERTY(meta=(BindWidget))
	class UButton* Sell;
	class AItemStore* itemStore;
	class UItemStoreWidget* itemStoreWidget;
	UPROPERTY()
	EWeaponSlot currentWeaponSlot;
	UPROPERTY()
	TMap<EWeaponSlot, class AWeaponBase*> equips;
	int32 selectWeaponIDX;
private: 
UFUNCTION(BlueprintCallable)
void testWeaponBuy();
UFUNCTION(BlueprintCallable)
void SellWeapon();
UFUNCTION(BlueprintCallable)
void SetNowEquipWeapon(EWeaponSlot WeaponSlot);
UFUNCTION(BlueprintCallable)
void SetWeaponSelect(int32 idx);
void ClosePopUPWidget();
};
