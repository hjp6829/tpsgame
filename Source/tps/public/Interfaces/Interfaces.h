// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Interfaces/Enums.h"
#include "Interfaces.generated.h"
UINTERFACE(MinimalAPI)
class UEquipable :public UInterface
{
	GENERATED_BODY()

};
class IEquipable
{
	GENERATED_BODY()
public:
	virtual float GetMuzzleEquipData() = 0;
	virtual int32 GetMagazineEquipData() = 0;
	virtual float GetStockEquipData() = 0;
	virtual ERangeWeaponAttachmentType GetEquipTypeData() = 0;
};
UINTERFACE(MinimalAPI)
class UInteractable :public UInterface
{
	GENERATED_BODY()

};

class IInteractable
{
	GENERATED_BODY()
public:
	virtual void Interact(class AMainPlayer* player) =0;
	virtual void InteractEnd() = 0;
};

UINTERFACE(MinimalAPI)
class UItemAction :public UInterface
{
	GENERATED_BODY()

};

class IItemAction
{
	GENERATED_BODY()
public:
	virtual void ItemAction(class ACustomCharacter* character)=0;
};
/**
 * 
 */
class TPS_API Interfaces
{
public:
	Interfaces();
	~Interfaces();
};
