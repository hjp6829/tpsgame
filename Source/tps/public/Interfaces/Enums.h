// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EWeaponSlot : uint8
{
	NONE UMETA(DisplayName = "None"),
    Main UMETA(DisplayName = "MainWeapon"),
	Secondary UMETA(DisplayName = "SecondaryWeapon"),
	Throwaway UMETA(DisplayName = "ThrowawayWeapon"),
};
UENUM(BlueprintType)
enum class EAttackType : uint8
{
    RangeWeapon UMETA(DisplayName = "RangeWeapon"),
	MeleeWeapon UMETA(DisplayName = "MeleeWeapon"),
};
UENUM(BlueprintType)
enum class EItemType : uint8
{
    Hill UMETA(DisplayName = "Hill"),
	Ammunition UMETA(DisplayName = "Ammunition"),
	Weapon UMETA(DisplayName = "Weapon"),
	WeaponEquip UMETA(DisplayName = "WeaponEquip"),
};
UENUM(BlueprintType)
enum class EPoolType : uint8
{
	SpawnitemBase UMETA(DisplayName = "SpawnitemBase"),
	Weapon UMETA(DisplayName = "Weapon"),
};
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Rifle UMETA(DisplayName = "Rifle"),
	Sniper UMETA(DisplayName = "Sniper"),
	Shotgun UMETA(DisplayName = "Shotgun"),
	Grenade UMETA(DisplayName = "Grenade")
};
UENUM(BlueprintType)
enum class EUIType : uint8
{
	None UMETA(DisplayName = "None"),
	ItemStore UMETA(DisplayName = "ItemStore"),
	HUD UMETA(DisplayName = "HUD"),
	Inventory UMETA(DisplayName = "Inventory"),
};
UENUM(BlueprintType)
enum class ERangeWeaponAttachmentType : uint8
{
	None UMETA(DisplayName = "None"),
	MuzzleAttachment UMETA(DisplayName = "MuzzleAttachment"),
	MagazineAttachment UMETA(DisplayName = "MagazineAttachment"),
	StockAttachment UMETA(DisplayName = "StockAttachment"),
};
/**
 * 
 */
class TPS_API Enums
{
public:
	Enums();
	~Enums();
};
