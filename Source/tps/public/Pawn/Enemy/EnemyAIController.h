// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "Perception/AIPerceptionTypes.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()
public:
	AEnemyAIController();
	virtual void OnPossess(APawn* pawn)override;
	UFUNCTION(BlueprintCallable)
	UBlackboardComponent* GetBlackBoard();
protected:
UFUNCTION()
virtual void BeginPlay()override;
virtual void Tick(float DeltaTime)override;
UFUNCTION()
void OntargetDetected(AActor* actor, FAIStimulus const Stimulus);

private:
	UPROPERTY(EditInstanceOnly, Category = "AI")
	class UBehaviorTreeComponent* behaviortreeComp;
	UPROPERTY(EditAnywhere, Category = "AI")
	class UBehaviorTree* tree;
	class UBlackboardComponent* blackboard;
	FTimerHandle UpdateDistanceHandle;
	class AActor* targetActor=nullptr;
	class APawn* ownerPawn;
	bool isTargetDetected;
private:
 UFUNCTION()
 void DistanceCalculation();
};
