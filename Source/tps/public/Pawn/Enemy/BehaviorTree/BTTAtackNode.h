// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTAtackNode.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TPS_API UBTTAtackNode : public UBTTaskNode
{
	GENERATED_BODY()
public: 
	UBTTAtackNode();
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory);
private:
  UPROPERTY()
  FTimerHandle OnAttackEndTimer;
  UBehaviorTreeComponent* treeComp;
  UPROPERTY(EditAnywhere)
float attackTimeMin=3.0f;
UPROPERTY(EditAnywhere)
float attackTimeMax=5.0f;
FTimerHandle onAttackTimerHandle;
class UEnemyAttackComonent* attackComp;
private: 
UFUNCTION()
void SetAttackEnd();
void AttackLoof();
};
