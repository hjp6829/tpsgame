// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTRandomRange.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UBTTRandomRange : public UBTTaskNode
{
	GENERATED_BODY()
public:
	UBTTRandomRange();
	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory);
private:
	UPROPERTY(EditAnywhere)
	int32 Probability;
};
