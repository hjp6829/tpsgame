// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "BTDReloadingCheck.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UBTDReloadingCheck : public UBTDecorator
{
	GENERATED_BODY()
public:
	UBTDReloadingCheck();
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)const override;
private:
	bool reloadingCheck=false;
	FTimerHandle reloadingHandle;
};
