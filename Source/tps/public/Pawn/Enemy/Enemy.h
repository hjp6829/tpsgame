// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/CustomCharacter.h"
#include "Enemy.generated.h"

UCLASS()
class TPS_API AEnemy : public ACustomCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)override;
	virtual FVector GetFireEndCheckVector(int32 dir)override;
	virtual FVector GetFireEndCheckStartPos()override;
		UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="text")
	class UArrowComponent* sightArrow;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:
	void Initialize();
	void HitReset();
private:
	class AEnemyAIController* aiController;

};
