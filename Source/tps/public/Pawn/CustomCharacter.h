// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CustomCharacter.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FCharacterOverlapHideWall,bool value);
DECLARE_MULTICAST_DELEGATE_OneParam(FCharacterHit, bool value);
DECLARE_MULTICAST_DELEGATE_TwoParams(FCharacterYawAndPitch, float yaw, float pitch);
UCLASS()
class TPS_API ACustomCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACustomCharacter();
	FCharacterYawAndPitch OnSetCharacterYawAndPitch;
	FCharacterOverlapHideWall OnCharacterOverlapHideWall;
	FCharacterHit OnCharacterHit;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
protected:
	TMap<UClass*, class UComponentBase*> _dicPairComponent;
	bool isHit=false;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	template<typename T>
	T* GetCharacterComponent();
	virtual FVector GetFireEndCheckVector(int32 dir)
	{
		return FVector::ZeroVector;
	}
	virtual FVector GetFireEndCheckStartPos(){ return FVector::ZeroVector; }
	virtual void AddCameraRotation(FRotator rotation){};
	virtual FVector GetMeleeAttackLocation(){return GetActorLocation();};
};

template<typename T>
inline T* ACustomCharacter::GetCharacterComponent()
{
	UClass* serviceType = T::StaticClass();
	if (_dicPairComponent.Contains(serviceType))
	{
		return Cast<T>(_dicPairComponent[serviceType]);
	}
	return nullptr;
}