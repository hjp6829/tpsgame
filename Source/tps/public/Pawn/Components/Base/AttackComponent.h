// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/ComponentBase.h"
#include "AttackComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FMeleeAttack,bool value);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UAttackComponent : public UComponentBase
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAttackComponent();
	UFUNCTION()
	virtual void InitializeComponent()override;
	virtual class AWeaponBase* GetCurrentWeapon();
	virtual void SetCurrentWeapon(class AWeaponBase* weapon);
	virtual void SetWeaponState(bool value);
	void SetNowAim(bool value);
	bool NowReloadingCheck();
	FMeleeAttack OnMeleeAttack;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void OnAttackWeaponStart();
	virtual void OnAttackWeaponRelease();
	virtual void ReloadWeapon();
	virtual void WeaponReloadEnd();
	UFUNCTION()
	void SetMeleeAttackMontage();
		UFUNCTION()
	void EndMeleeAttackMontage(UAnimMontage* Montage, bool bInterrupted);
	UFUNCTION(BlueprintCallable)
	void MakeWeaponMeleeCollider(bool value);
protected:
	class AWeaponBase* CurrentWeapon;
	FTimerHandle OnReloadTimer;
	float ownerYaw;
	float ownerPitch;
	bool isAim;
	bool isReloading;
	class ACustomCharacter* owerCharacter;
private:
	void SetPlayerYawAndPitch(float yaw, float pitch);

};
