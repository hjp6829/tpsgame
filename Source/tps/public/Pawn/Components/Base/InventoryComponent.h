// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/ComponentBase.h"
#include "Item/ItemBase.h"
#include "InventoryComponent.generated.h"

USTRUCT(Atomic,BlueprintType)
struct FInventorySlot
{
	GENERATED_BODY()
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UItemBase* item=nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 inventoryIDX=0;
public:
	int32 GetMaxOverlapNumber() const
	{
		return item->getMaxOverlapNumber();
	}
	int32 GetItemIDX() const
	{
		return item->GetIDX();
	}
};

DECLARE_MULTICAST_DELEGATE(FIventoryUpdate);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UInventoryComponent : public UComponentBase
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInventoryComponent();
	virtual void AddItem(class UItemBase* itembase);
	TArray<FInventorySlot*>& GetInventory();
	void UseItem(int32 idx, int32 count);
	int32 GetItemNum(int32 idx);
	TMap<EWeaponSlot, class AWeaponBase*>& GetWeaponSlot();
	void SetNULLIDX(int32 idx);
public:
	FIventoryUpdate OnInventoryUpdate;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
protected:
	TMap<EWeaponSlot, class AWeaponBase*> EquipedWeapon;
	EWeaponSlot currentWeaponSlot;
	class AWeaponFactory* weaponFactory;
	TArray<FInventorySlot*> inventory;
	int32 ThrowWeaponCount=0;
private:

	void AddNewItem(int32 itemIDX, int32 num);
};
