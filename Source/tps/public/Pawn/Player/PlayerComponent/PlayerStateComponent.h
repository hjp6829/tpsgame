// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/Player/PlayerComponent/StateComponent.h"
#include "PlayerStateComponent.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UPlayerStateComponent : public UStateComponent
{
	GENERATED_BODY()
	public:
		virtual void BeginPlay() override;
		UPlayerStateComponent();
		void UseGold(int32 value);
	protected:
	virtual void InitializeComponent()override;
};
