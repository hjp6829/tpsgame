// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/ComponentBase.h"
#include "PlayerMoveComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FCoverCheck,bool value);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPlayerMoveComponent : public UComponentBase
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerMoveComponent();
	void SetRunAndWalkSpeed(float newWalkSpeed,float newRunSpeed);
public:
	FCoverCheck OnCoverCheck;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	UFUNCTION()
	virtual void InitializeComponent()override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
private: 
	UPROPERTY()
	class UCharacterMovementComponent* characterMoveComp;
	UFUNCTION()
	void RunStateChange(bool value);
	void WallTrace(bool value);
	void MoveCharacter(FVector2D value);
	void CoverTrace(FVector2D playeRightVector);
	void SetIsAim(bool value);
private: 
	UPROPERTY()
	float walkSpeed;
	UPROPERTY()
	float runSpeed;
	class AMainPlayer* player;
	bool isInCover=false;
	bool isAim;
//#pragma region debug
//	FVector rightOwnerpos;
//	FVector rightEndPos;
//	FVector leftOwnerpos;
//	FVector leftEndPos;
//#pragma endregion

	
};
