// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/ComponentBase.h"
#include "StateComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FRunToggle,bool value);
DECLARE_MULTICAST_DELEGATE_TwoParams(FHPUpdate,int32 value1,int32 value2);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UStateComponent : public UComponentBase
{
	GENERATED_BODY()

public:	
	void SetIsRun(bool value);
	void HillCharacter(int32 value);
	int32 GetCharacterGold();
	int32 GetMaxHP();
	int32 GetHP();
public:
	FRunToggle OnRunToggle;
	FHPUpdate OnHPUpdate;
protected:
	void SetIsAim(bool value);
protected:
	int32 maxHp=0;
	float walkSpeed=0;
	float runSpeed=0;
	int32 gold=0;
	UPROPERTY()
	bool isAim=false;
	UPROPERTY()
	bool isRun=false;
	int32 currentHP=0;
};
