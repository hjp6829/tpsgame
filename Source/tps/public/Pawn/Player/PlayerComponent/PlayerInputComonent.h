// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/ComponentBase.h"
#include "PlayerInputComonent.generated.h"

DECLARE_DELEGATE_OneParam(FAttackBinding,bool value);
DECLARE_MULTICAST_DELEGATE_OneParam(FEqipChangeing,EWeaponSlot WeaponSlot);
DECLARE_MULTICAST_DELEGATE(FReloding);
DECLARE_MULTICAST_DELEGATE_OneParam(FMouseAim, bool value);
DECLARE_DELEGATE(FInteraction);
DECLARE_MULTICAST_DELEGATE_OneParam(FToggleInventoryUI, bool value);
DECLARE_MULTICAST_DELEGATE_OneParam(FToggleBuild, bool value);
DECLARE_DELEGATE(FBuildActor);
DECLARE_DELEGATE_OneParam(FBuildRotateActor,bool value);
DECLARE_MULTICAST_DELEGATE_OneParam(FCoverCheck, bool value);
DECLARE_MULTICAST_DELEGATE_OneParam(FCronchCheck, bool value);
DECLARE_MULTICAST_DELEGATE_OneParam(FMoveCharacter, FVector2D value);
DECLARE_MULTICAST_DELEGATE(FWeaponMeleeAttack);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UPlayerInputComonent : public UComponentBase
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPlayerInputComonent();
	FAttackBinding OnAttackInput;
	FEqipChangeing OnEquipChange;
	FReloding OnReloading;
	FMouseAim OnMouseAim;
	FInteraction OnInteraction;
	FToggleInventoryUI OnToggleInventoryWidget;
	FToggleBuild OnToggleBuild;
	FBuildActor OnBuildActor;
	FBuildRotateActor OnBuildRotateActor; 
	FCoverCheck OnCover;
	FMoveCharacter OnMoveCharacter;
	FCronchCheck OnCronuch;
	FWeaponMeleeAttack OnMeleeAttack;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	class AMainPlayer* ownerPlayer;
public:	
	UFUNCTION()
	virtual void InitializeComponent()override;
private:
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* mouseInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* moveInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* equipChange1Input;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* equipChange2Input;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* mouseLecftClick;
		UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* mouseRightClick;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* reloadingInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* runInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* interactionInput;
		UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* throwawayInput;
			UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* inventoryInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* buildInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* buildRotateInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* coverInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* cronchInput;
	UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputAction* weaponMeleeAttackInput;
	bool isAim=false;
	bool isMove=false;
	bool isInteraction=false;
	bool isCanMove=true;
	bool isCanLook=true;
	bool isInventoryWidgetOpen=false;
	bool isBuildMode=false;
	bool isBuildRotate=false;
	bool coverToggle=false;
	bool isCoverCheck=false;
	bool isCronch=false;
	bool isMeleeAttack=false;
private:
	void SetUpInputBinding(class UEnhancedInputComponent* input);
	UFUNCTION()
void Look(const FInputActionValue& value);
UFUNCTION()
void Move(const FInputActionValue& value);
UFUNCTION()
void RunStart(const FInputActionValue& value);
UFUNCTION()
void RunRelase(const FInputActionValue& value);
UFUNCTION()
void BuildStart(const FInputActionValue& value);
UFUNCTION()
void EquipInputMain(const FInputActionValue& value);
UFUNCTION()
void EquipInputSecondary(const FInputActionValue& value);
UFUNCTION()
void EquipChange(EWeaponSlot WeaponSlot);
UFUNCTION()
void BuildRotate(const FInputActionValue& value);
UFUNCTION()
void OnTogglrInventory(const FInputActionValue& value);
UFUNCTION()
void OnMouseLeftClickPressed(const FInputActionValue& value);
UFUNCTION()
void OnMouseLeftClickReleased(const FInputActionValue& value);
UFUNCTION()
void OnMouseRightClickPressed(const FInputActionValue& value);
UFUNCTION()
void OnMouseRightClickReleased(const FInputActionValue& value);
UFUNCTION()
void OnReloadingClick(const FInputActionValue& value);
UFUNCTION()
void SetControllerRotationYawTrue(const FInputActionValue& value);
UFUNCTION()
void SetControllerRotationYawFalse(const FInputActionValue& value);
UFUNCTION()
void InteractionItem(const FInputActionValue& value);
UFUNCTION()
void OnThrowawayChange(const FInputActionValue& value);
UFUNCTION()
void OnCoverCheck(const FInputActionValue& value);
UFUNCTION()
void OnWeaponMeleeAttack(const FInputActionValue& value);
UFUNCTION()
void SetInteractionItem(bool value);
UFUNCTION()
void SetIsLookInput(bool value);
UFUNCTION()
void SetIsMoveInput(bool value);
UFUNCTION()
void SetIsCover(bool value);
UFUNCTION()
void SetMeleeAttack(bool value);
UFUNCTION()
void SetIsCronch(const FInputActionValue& value);
};
