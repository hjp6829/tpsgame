// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/ComponentBase.h"
#include "BuildSystemComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UBuildSystemComponent : public UComponentBase
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBuildSystemComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
private:
	void BuildPreview();
	void SetUpBuildActor();
	void SetBuildPreviewRotate(bool value);
	void SetPreviewRotation();
private:
    class AMainPlayer* player;
	UPROPERTY(EditAnywhere)
	TSubclassOf<class AActor> previewActorClass;
	class AActor* previewActor= nullptr;
	void SetPreviewActor(bool value);
	bool isBuildMode;
	FTimerHandle rotationHandle;
};
