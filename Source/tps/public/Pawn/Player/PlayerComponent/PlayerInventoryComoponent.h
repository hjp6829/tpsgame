// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/Components/Base/InventoryComponent.h"
#include "PlayerInventoryComoponent.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API UPlayerInventoryComoponent : public UInventoryComponent
{
	GENERATED_BODY()
public:
	TMap<EWeaponSlot, class AWeaponBase*>& GetEquipWeaponMap();
	void SetWeaponEquip(EWeaponSlot WeaponSlot, class AWeaponBase* newWeapon);
	virtual void AddItem(class UItemBase* item)override;
protected:
	UFUNCTION()
	virtual void InitializeComponent()override;
	virtual void BeginPlay()override;
private:
	UFUNCTION(BlueprintCallable)
	void ChangeEquip(EWeaponSlot WeaponSlot);
	UFUNCTION(BlueprintCallable)
	void SetWeaponEquip(EWeaponSlot WeaponSlot, EWeaponType weapontype);
	UFUNCTION()
	void TestFuntion(int32 idx, int32 value);
	UFUNCTION()
	void ReloadBulletUpdate(int32 idx, int32 value);
	UFUNCTION()
	void Initialize();
	UFUNCTION()
	void UpdateWeaponMagazine();
private:
class UPlayerHUD* playerHUD;
};
