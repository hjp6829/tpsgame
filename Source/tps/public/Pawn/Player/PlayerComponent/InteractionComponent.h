// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/ComponentBase.h"
#include "InteractionComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FCanInteraction, bool value);
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_API UInteractionComponent : public UComponentBase
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInteractionComponent();
	FCanInteraction OnCanInteraction;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void InitializeComponent()override;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
private:
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypes;
	AActor* currentInteractActor;
	UPROPERTY(EditAnywhere)
	float ViewAngle=90;
private:
	void InteractionObject();
};
