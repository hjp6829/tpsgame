// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pawn/CustomCharacter.h"
#include "InputActionValue.h"
#include "MainPlayer.generated.h"


DECLARE_MULTICAST_DELEGATE_OneParam(FInputBindingDelegate, UEnhancedInputComponent*);
UCLASS()
class TPS_API AMainPlayer : public ACustomCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainPlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual FVector GetFireEndCheckVector(int32 dir) override;
	virtual FVector GetFireEndCheckStartPos()override;
	void SetHUDCrossHair(bool value);
	void SetSpringArmAim(bool value);
	UFUNCTION()
	UPlayerHUD* GetPlayerHUD();
	  UFUNCTION()
    void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor,
                        class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
                        bool bFromSweep, const FHitResult& SweepResult);
	  UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)override;
	void TogglePlayerInventory();
	virtual void AddCameraRotation(FRotator rotation)override;
	virtual FVector GetMeleeAttackLocation()override;
public:
	FInputBindingDelegate OnInputBindingDelegate;
private: 
	UPROPERTY(EditAnywhere, Category = "Camera")
	class UCameraComponent* playerCameraComp;
	UPROPERTY(EditAnywhere, Category = "Camera")
	class USpringArmComponent* springArmComp;
		UPROPERTY(VisibleAnywhere,Category="Input")
	class UInputMappingContext* defaultContext;
	class UPlayerHUD* playerHUD;
	UPROPERTY(VisibleAnywhere)
	class UBuildSystemComponent* buildComp;
	bool isCover=false;
	bool isCronch=false;
private: 
UFUNCTION()
	void SetIsCover(bool value);
	UFUNCTION()
	void SetIsCronch(bool value);
};

