// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "PlayerAnimInstance.generated.h"
DECLARE_MULTICAST_DELEGATE_OneParam(FMeleeAttackCollision,bool value);
/**
 * 
 */
UCLASS()
class TPS_API UPlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
public:
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="PlayerAnim")
	float Speed=0;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="PlayerAnim")
	float Direction=0;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="PlayerAnim")
	float AimPitch=0;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="PlayerAnim")
	float AimYaw =0;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="PlayerAnim")
	bool isInAir=false;
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="PlayerAnim")
	bool isCrouch=false;
		UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category="PlayerAnim")
	bool isAim=false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PlayerAnim")
	bool isCoverJump = false;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PlayerAnim")
	bool isHit = false;
	class ACustomCharacter* ownerCharacter;
	FMeleeAttackCollision OnMeleeAttackCollision;
public:
	virtual void NativeUpdateAnimation(float DeltaSeconds)override;
	virtual void NativeBeginPlay()override;
private:
	void SetPlayerYawAndPitch(float yaw,float pitch);
		UFUNCTION()
	void SetCrouch(bool value);
	bool isCover=false;
	UFUNCTION()
	void SetIsAim(bool value);
			UFUNCTION()
	void SetCover(bool value);
			UFUNCTION()
	void SetHit(bool value);
	UFUNCTION()
	void AnimNotify_AttackStart();
	UFUNCTION()
	void AnimNotify_AttackEnd();
	UPROPERTY(EditAnywhere)
	class UAnimMontage* hitMontage;
};
