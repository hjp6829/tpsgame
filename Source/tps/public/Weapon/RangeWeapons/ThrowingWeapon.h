// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "ThrowingWeapon.generated.h"

UCLASS()
class TPS_API AThrowingWeapon : public ARangedWeaponBase
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AThrowingWeapon();
	virtual void AttackStart(float yaw, float pitch) override;
	virtual void AttackRelease() override;
public:
	UPROPERTY(EditAnywhere)
	TSubclassOf<class AGrenadeBullet> bulletClsss;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
private:
 float speed=100;
 FRotator launchDirection;
};
