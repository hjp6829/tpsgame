// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "RifleWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API ARifleWeapon : public ARangedWeaponBase
{
	GENERATED_BODY()
public:
	ARifleWeapon();
	virtual void FireBullet() override;
	virtual void AttackStart(float yaw, float pitch) override;
	virtual void AttackRelease() override;
protected:
	virtual void BeginPlay()override;
private: 
 bool isAttack;
 FTimerHandle StartSpreadRadiusDecreaseHandle;
 FTimerHandle SpreadRadiusDecreaseHandle;
private:
 void SetCanAttack();
 void StartSpreadRadiusDecrease();
 void SpreadRadiusDecrease();
};
