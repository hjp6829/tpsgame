// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/RangeWeapons/RangedWeaponBase.h"
#include "ShotgunWeapon.generated.h"

/**
 * 
 */
UCLASS()
class TPS_API AShotgunWeapon : public ARangedWeaponBase
{
	GENERATED_BODY()
public:
	AShotgunWeapon();
	virtual void FireBullet() override;
	virtual void AttackStart(float yaw, float pitch) override;
	virtual void AttackRelease() override;
protected:
	virtual void BeginPlay()override;
private:
UPROPERTY(EditAnywhere)
 TSubclassOf<class ABullet> bulletClass;
 UPROPERTY(EditAnywhere)
 int32 buckshotCount=0;
	bool isAttack;
	FTimerHandle StartSpreadRadiusDecreaseHandle;
	FTimerHandle SpreadRadiusDecreaseHandle;
private:
	void SetCanAttack();
	void StartSpreadRadiusDecrease();
	void SpreadRadiusDecrease();
};
